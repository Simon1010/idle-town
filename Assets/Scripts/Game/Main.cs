﻿using Game.Ecs;
using Game.Ecs.View;
using UnityEngine;

namespace Game
{
    public class Main : MonoBehaviour
    {
        private EcsController _controller;

        // Start is called before the first frame update
        void Start()
        {
            _controller = new EcsController();


            var contextUsers = GetComponentsInChildren<IContextUser>();
            foreach (var contextUser in contextUsers)
            {
                contextUser.SupplyContext(_controller.Contexts);
            }

            var spawners = GetComponentsInChildren<IViewSpawner>();
            foreach (var viewSpawner in spawners)
            {
                viewSpawner.Initialize(spawners);
            }


            _controller.Initialize();
        }


        // Update is called once per frame
        void Update()
        {
            _controller.Update();
        }
    }
}