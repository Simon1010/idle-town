using System;
using DigitalRuby.Tween;
using Game.Ecs;
using Game.Unity.Spawners;
using UnityEngine;
using Utilities;

namespace Game.Unity.Views
{
    public class PlayerView : View, IListenerPosition, IListenerPlayerZoom, IListenerPlayerCurrentPlot
    {
        private CameraController _cameraController;
        private Action<ITween<Vector3>> _positionTween;
        private float TweenSpeed = 0.3f;
        private Action<ITween<Vector3>> _onCompletePosition;

        private void Awake()
        {
            _cameraController = GetComponentInChildren<CameraController>();
        }

        protected override void OnInitalized()
        {
            Listener.AddListenerPosition(this);
            Listener.AddListenerPlayerCurrentPlot(this);
            Listener.AddListenerPlayerZoom(this);
            _positionTween = t => transform.localPosition = t.CurrentValue;
            _onCompletePosition = t => { _cameraController.RefreshZoom(); };
        }

        protected override void OnDestroyed()
        {
        }

        public void OnPosition(System.Numerics.Vector3 value)
        {
            TweenFactory.Tween("PlayerPosition", transform.localPosition, value.ToUnityVector3(), TweenSpeed,
                TweenScaleFunctions.CubicEaseOut,
                _positionTween, _onCompletePosition);
        }

        public void OnPlayerZoom(float value)
        {
//            _cameraController.SetZoom(value);
        }

        private void Update()
        {
            if (Input.GetMouseButtonUp(1))
            {
                GetEntity().RemovePlayerCurrentPlot();
                _cameraController.Target = null;
            }
        }

        public void OnPlayerCurrentPlot(CoreEntityId value)
        {
            var plotView = GameSpawnerManager.Instance.GetSpawner<PlotSpawner>().Get(value);
            _cameraController.Target = plotView.GetComponent<ICameraFocuseable>();
        }
    }
}