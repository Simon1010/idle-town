using System;
using Game.Ecs;
using Game.Unity.Spawners;
using UnityEngine;
using Utilities;
using Random = UnityEngine.Random;

namespace Game.Unity.Views
{
    public class PlotView : View, IListenerPosition, ICameraFocuseable, IListenerAnyBuildingPlot
    {
        private Vector3 _buildingCenterOffset = new Vector3(0, 1, 0);
        private BuildingView _currentBuilding;
        private bool _isOver;

        private void Awake()
        {
            GetComponentInChildren<MeshRenderer>().material.color = Random.ColorHSV();
        }

        protected override void OnInitalized()
        {
            Listener.AddListenerPosition(this);
            Listener.AddListenerAnyBuildingPlot(this);
        }

        protected override void OnDestroyed()
        {
        }

        public void OnPosition(System.Numerics.Vector3 value)
        {
            transform.localPosition = value.ToUnityVector3();
        }

        private void OnMouseEnter()
        {
            _isOver = true;
        }

        private void OnMouseExit()
        {
            _isOver = false;
        }

        private void Update()
        {
            if (Input.GetMouseButtonUp(0))
            {
                if (_isOver)
                {
                    Contexts.core.playerEntity.ReplacePlayerCurrentPlot(EntityId);
                }
            }
        }


        public void OnAnyBuildingPlot(CoreEntity coreEntity, CoreEntityId value)
        {
            if (value == EntityId)
            {
                _currentBuilding = GameSpawnerManager.Instance.GetSpawner<BuildingSpawner>()
                    .Get(coreEntity.coreId.value).GetComponent<BuildingView>();
            }
        }

        public Vector3 Position
        {
            get
            {
                if (_currentBuilding == null)
                {
                    return transform.position;
                }

                return _currentBuilding.Position;
            }
        }

        public Vector3 EulerAngles
        {
            get => _currentBuilding.EulerAngles;
            set => _currentBuilding.EulerAngles = value;
        }

        public float GetMinZoom()
        {
            return _currentBuilding.GetMinZoom();
        }

        public float GetMaxZoom()
        {
            return _currentBuilding.GetMaxZoom();
        }
    }
}