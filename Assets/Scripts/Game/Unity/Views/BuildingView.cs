using Game.Ecs;
using UnityEngine;

namespace Game.Unity.Views
{
    public class BuildingView : View, IListenerPosition, ICameraFocuseable
    {
        public float minZoom;
        public float maxZoom;
        public Transform BuildingCenterOffset;
        public Vector3 Position => transform.position + BuildingCenterOffset.transform.localPosition;

        public Vector3 EulerAngles
        {
            get { return transform.eulerAngles; }
            set { transform.eulerAngles = value; }
        }

        public float GetMinZoom()
        {
            return minZoom;
        }

        public float GetMaxZoom()
        {
            return maxZoom;
        }

        protected override void OnInitalized()
        {
            Listener.AddListenerPosition(this);
        }

        protected override void OnDestroyed()
        {
        }

        public void OnPosition(System.Numerics.Vector3 value)
        {
            transform.localPosition = value.ToUnityVector3();
        }
    }
}