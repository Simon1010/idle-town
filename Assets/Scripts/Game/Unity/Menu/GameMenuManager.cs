using UnityEngine;

namespace Game.Unity.Menu
{
    public static class GameMenuManager
    {
        private static MenuManager _instance;

        public static MenuManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    var go = GameObject.Find("Game Menu Manager");

                    if (go == null)
                    {
                        go = new GameObject("Game Menu Manager");
                        go.transform.SetParent(GameObject.Find("Screen Canvas").transform);
                        go.transform.localPosition = Vector3.zero;
                        go.AddComponent<MenuManager>();
                    }

                    _instance = go.GetComponent<MenuManager>();
                }

                return _instance;
            }
        }
    }
}