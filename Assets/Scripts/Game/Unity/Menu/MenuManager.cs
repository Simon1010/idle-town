using System;
using Game.Ecs;
using UnityEngine;
using Utilities;

namespace Game.Unity.Menu
{
    public class MenuManager : MonoBehaviour, IContextUser
    {
        private Contexts _contexts;
        private Menu _currentMenu;
        private ListenerEntity _listener;

        public void SupplyContext(Contexts contexts)
        {
            _contexts = contexts;
            _listener = contexts.listener.CreateEntity();
        }

        public void CreateMenu(GameEnums.MenuId menuId)
        {
            CloseCurrentMenu();

            GameObject prefab = null;

            switch (menuId)
            {
                case GameEnums.MenuId.Main:
                    break;
                case GameEnums.MenuId.Settings:
                    prefab = GameResourceManager.Instance.GetPrefab("Settings Menu");
                    break;
                case GameEnums.MenuId.WorkerManagement:
                    prefab = GameResourceManager.Instance.GetPrefab("Worker Management Menu");
                    break;
                case GameEnums.MenuId.ProductManagement:
                    prefab = GameResourceManager.Instance.GetPrefab("Product Management Menu");
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(menuId), menuId, null);
            }

            var menuGO = Instantiate(prefab, transform);
            menuGO.transform.localPosition = Vector3.zero;
            var menu = menuGO.GetComponent<Menu>();
            menu.Initialize(_contexts);
            menu.Setup();

            _currentMenu = menu;
        }

        public void CloseCurrentMenu()
        {
            _currentMenu?.DestroyMenu();
            _currentMenu = null;
        }
    }
}