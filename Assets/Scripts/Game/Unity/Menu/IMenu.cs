namespace Game.Unity.Menu
{
    public interface IMenu
    {
        void Initialize(Contexts contexts);
        void DestroyMenu();
    }
}