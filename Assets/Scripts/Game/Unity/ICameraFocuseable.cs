using UnityEngine;

namespace Game.Unity
{
    public interface ICameraFocuseable
    {
        Vector3 Position { get; }
        Vector3 EulerAngles { get; set; }
        float GetMinZoom();
        float GetMaxZoom();
    }
}