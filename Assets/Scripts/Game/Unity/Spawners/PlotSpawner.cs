using UnityEngine;
using Utilities;

namespace Game.Unity.Spawners
{
    public class PlotSpawner : ViewSpawner, IListenerAnyPlot
    {
        protected override void OnInitalized()
        {
            Listener.AddListenerAnyPlot(this);
        }

        public void OnAnyPlot(CoreEntity coreEntity, bool isPlot)
        {
            Spawn(GameResourceManager.Instance.GetPrefab("Plot"), coreEntity);
        }
    }
}