using System;
using UnityEngine;
using Utilities;

namespace Game.Unity.Spawners
{
    public class BuildingSpawner : ViewSpawner, IListenerAnyBuilding
    {
        protected override void OnInitalized()
        {
            Listener.AddListenerAnyBuilding(this);
        }

        public void OnAnyBuilding(CoreEntity coreEntity, bool isBuilding)
        {
            var prefabName = String.Empty;
            switch (coreEntity.buildingId.value)
            {
                case GameEnums.BuildingId.ShedSmall:
                    prefabName = "Shed Small";
                    break;
                case GameEnums.BuildingId.BarnSmall:
                    prefabName = "Barn Small";
                    break;
                case GameEnums.BuildingId.FoodCart:
                    prefabName = "Food Cart";
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            Spawn(GameResourceManager.Instance.GetPrefab(prefabName), coreEntity);
            coreEntity.ReplaceBuildingPlot(coreEntity.buildingPlot.value);
        }
    }
}