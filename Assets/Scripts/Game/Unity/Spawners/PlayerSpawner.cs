using Utilities;

namespace Game.Unity.Spawners
{
    public class PlayerSpawner : ViewSpawner, IListenerAnyPlayer
    {
        protected override void OnInitalized()
        {
            Listener.AddListenerAnyPlayer(this);
        }

        public void OnAnyPlayer(CoreEntity coreEntity, bool isPlayer)
        {
            Spawn(GameResourceManager.Instance.GetPrefab("Player"), coreEntity);
        }
    }
}