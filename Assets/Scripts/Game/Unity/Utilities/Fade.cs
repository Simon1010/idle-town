using System;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Utilities
{
    public class Fade : MonoBehaviour
    {
        public SpriteRenderer[] SpriteRenderers;
        public TMP_Text[] TextRenderers;

        public float FadeAcceleration = 0.2f;
        public float AlphaStart;
        public float AlphaEnd = 1f;
        public float WaitTime = 1f;

        public bool DestroyWhenDone = false;
        private bool _isFinished = false;


        private void Start()
        {
            if (SpriteRenderers == null || SpriteRenderers.Length == 0)
            {
                SpriteRenderers = GetComponentsInChildren<SpriteRenderer>();
            }

            if (TextRenderers == null || TextRenderers.Length == 0)
            {
                TextRenderers = GetComponentsInChildren<TMP_Text>();
            }

            SetAlpha(AlphaStart);
        }

        private void SetAlpha(float alpha)
        {
            foreach (var spriteRenderer in SpriteRenderers)
            {
                var color = spriteRenderer.color;
                color.a = alpha;
                spriteRenderer.color = color;
            }

            foreach (var textRenderer in TextRenderers)
            {
                var color = textRenderer.color;
                color.a = alpha;
                textRenderer.color = color;
            }
        }

        private void Update()
        {
            if (WaitTime > 0)
            {
                WaitTime -= Time.deltaTime;
                return;
            }

            var color = Color.white;

            if (SpriteRenderers != null && SpriteRenderers.Length > 0)
            {
                color = SpriteRenderers.OrderByDescending(e => e.color.a).FirstOrDefault().color;
            }

            if (TextRenderers != null && TextRenderers.Length > 0)
            {
                color = TextRenderers.OrderByDescending(e => e.color.a).FirstOrDefault().color;
            }

            if (_isFinished)
            {
                color.a = AlphaEnd;

                if (DestroyWhenDone)
                {
                    GameObject.Destroy(gameObject);
                }

                return;
            }


            if (AlphaStart < AlphaEnd)
            {
                _isFinished = color.a >= AlphaEnd;
                color.a += FadeAcceleration;
            }
            else
            {
                _isFinished = color.a <= AlphaEnd;
                color.a -= FadeAcceleration;
            }


            SetAlpha(color.a);
        }
    }
}