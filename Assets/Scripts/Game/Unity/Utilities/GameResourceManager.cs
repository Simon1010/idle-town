using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Utilities
{
    public class GameResourceManager : MonoBehaviour
    {
        private static GameResourceManager _instance;

        public static GameResourceManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    var go = GameObject.Find("Game Resource Manager");
                    _instance = go != null
                        ? go.GetComponent<GameResourceManager>()
                        : new GameObject("Game Resource Manager").AddComponent<GameResourceManager>();
                }

                return _instance;
            }
        }

        public Sprite[] AllSprites;
        public GameObject[] AllPrefabs;
        public AudioClip[] AllSounds;

        private Dictionary<string, Sprite> _allSprites;
        private Dictionary<string, GameObject> _allPrefabs;
        private Dictionary<string, AudioClip> _allSounds;

        private void Awake()
        {
            Instance.AllPrefabs = Resources.LoadAll<GameObject>("Prefabs");
            Instance.AllSounds = Resources.LoadAll<AudioClip>("Sounds");

            _allSprites = new Dictionary<string, Sprite>();
            foreach (var sprite in AllSprites)
            {
                _allSprites.Add(sprite.name, sprite);
            }

            _allPrefabs = new Dictionary<string, GameObject>();
            foreach (var prefab in AllPrefabs)
            {
                _allPrefabs.Add(prefab.name, prefab);
            }
            
            
            _allSounds = new Dictionary<string, AudioClip>();
            foreach (var sound in AllSounds)
            {
                _allSounds.Add(sound.name, sound);
            }
        }


#if UNITY_EDITOR
        [MenuItem("Siege/Update Assets")]
        public static void UpdateAssets()
        {
            var sprites = new List<Sprite>();
            var sheetNames = new[] {"sheet_01", "sheet_02"};
            foreach (var sheetName in sheetNames)
            {
                sprites.AddRange(GetAll<Sprite>("Assets/Art/" + sheetName + ".png"));
            }

            Instance.AllSprites = sprites.ToArray();
        }

        private static T[] GetAll<T>(string path)
        {
            var assets = AssetDatabase.LoadAllAssetsAtPath(path);
            var typeArray = assets.Where(e => e is T).Cast<T>();
            return typeArray.ToArray();
        }
#endif

        public Sprite GetSprite(string path)
        {
            if (_allSprites.ContainsKey(path) == false)
            {
                Debug.LogError("Sprite not found " + path);
                return null;
            }

            return _allSprites[path];
        }


        public GameObject GetPrefab(string prefabPath)
        {
            return _allPrefabs[prefabPath];
        }

        public T GetPrefab<T>(string prefabPath)
        {
            return GetPrefab(prefabPath).GetComponent<T>();
        }

        public AudioClip GetSound(string name)
        {
            return _allSounds[name];
        }
    }
}