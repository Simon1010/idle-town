using Game.Ecs.Commands;
using UnityEngine;

namespace Game.Unity
{
    public class SwipeEmitter : MonoBehaviour
    {
        private Vector2 fingerDown;
        private Vector2 fingerUp;
        public bool detectSwipeOnlyAfterRelease = false;

        public float SWIPE_THRESHOLD = 20f;

        // Update is called once per frame
        void Update()
        {
            if (Application.isMobilePlatform)
            {
                foreach (Touch touch in Input.touches)
                {
                    Debug.Log(Input.touches);
                    if (touch.phase == TouchPhase.Began)
                    {
                        fingerUp = touch.position;
                        fingerDown = touch.position;
                    }

                    //Detects Swipe while finger is still moving
                    if (touch.phase == TouchPhase.Moved)
                    {
                        if (!detectSwipeOnlyAfterRelease)
                        {
                            fingerDown = touch.position;
                            checkSwipe();
                        }
                    }

                    //Detects swipe after finger is released
                    if (touch.phase == TouchPhase.Ended)
                    {
                        fingerDown = touch.position;
                        checkSwipe();
                    }
                }
            }
            else
            {
                if (Input.GetMouseButtonDown(0))
                {
                    fingerUp = Input.mousePosition;
                    fingerDown = Input.mousePosition;
                }

                //Detects Swipe while finger is still moving
                if (Input.GetMouseButton(0))
                {
                    if (!detectSwipeOnlyAfterRelease)
                    {
                        fingerDown = Input.mousePosition;
                        checkSwipe();
                    }
                }

                //Detects swipe after finger is released
                if (Input.GetMouseButtonUp(0))
                {
                    fingerDown = Input.mousePosition;
                    checkSwipe();
                }
            }
        }

        void checkSwipe()
        {
            //Check if Vertical swipe
            if (verticalMove() > SWIPE_THRESHOLD && verticalMove() > horizontalValMove())
            {
                //Debug.Log("Vertical");
                if (fingerDown.y - fingerUp.y > 0) //up swipe
                {
                    OnSwipeUp();
                }
                else if (fingerDown.y - fingerUp.y < 0) //Down swipe
                {
                    OnSwipeDown();
                }

                fingerUp = fingerDown;
            }

            //Check if Horizontal swipe
            else if (horizontalValMove() > SWIPE_THRESHOLD && horizontalValMove() > verticalMove())
            {
                //Debug.Log("Horizontal");
                if (fingerDown.x - fingerUp.x > 0) //Right swipe
                {
                    OnSwipeRight();
                }
                else if (fingerDown.x - fingerUp.x < 0) //Left swipe
                {
                    OnSwipeLeft();
                }

                fingerUp = fingerDown;
            }

            //No Movement at-all
            else
            {
                //Debug.Log("No Swipe!");
            }
        }

        float verticalMove()
        {
            return Mathf.Abs(fingerDown.y - fingerUp.y);
        }

        float horizontalValMove()
        {
            return Mathf.Abs(fingerDown.x - fingerUp.x);
        }

        //////////////////////////////////CALLBACK FUNCTIONS/////////////////////////////
        void OnSwipeUp()
        {
            GameInputActions.CreateSwipe(Contexts.sharedInstance, new System.Numerics.Vector2(0, 1));
        }

        void OnSwipeDown()
        {
            GameInputActions.CreateSwipe(Contexts.sharedInstance, new System.Numerics.Vector2(0, -1));
        }

        void OnSwipeLeft()
        {
            GameInputActions.CreateSwipe(Contexts.sharedInstance, new System.Numerics.Vector2(1, 0));
        }

        void OnSwipeRight()
        {
            GameInputActions.CreateSwipe(Contexts.sharedInstance, new System.Numerics.Vector2(-1, 0));
        }
    }
}