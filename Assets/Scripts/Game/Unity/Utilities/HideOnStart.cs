using UnityEngine;

namespace Utilities
{
    public class HideOnStart : MonoBehaviour
    {
        private void Start()
        {
            gameObject.SetActive(false);
        }
    }
}