using System;
using UnityEngine;

namespace Utilities
{
    public class ScaleIn : MonoBehaviour
    {
        public Transform Transform;
        public float ScaleAcceleration = 0.2f;
        public Vector3 ScaleStart;

        private void Awake()
        {
            if (Transform == null)
            {
                Transform = transform;
            }

            Transform.localScale = ScaleStart;
        }

        private void Update()
        {
            var scale = Transform.localScale;
            if (scale.x > 1)
            {
                scale.x = 1;
            }

            if (scale.y > 1)
            {
                scale.y = 1;
            }

            if (scale.x == 1 && scale.y == 1)
            {
                return;
            }

            scale.x += ScaleAcceleration;
            scale.y += ScaleAcceleration;
            Transform.localScale = scale;
        }
    }
}