using System;
using System.Collections.Generic;
using Game.Ecs.View;
using UnityEngine;

namespace Utilities
{
    public class GameSpawnerManager : MonoBehaviour
    {
        private static GameSpawnerManager _gameSpawnerManager;

        public static GameSpawnerManager Instance
        {
            get
            {
                if (_gameSpawnerManager == null)
                {
                    _gameSpawnerManager = GameObject.Find("Main").GetComponentInChildren<GameSpawnerManager>();
                }

                return _gameSpawnerManager;
            }
        }

        private Dictionary<System.Type, IViewSpawner> _spawners;

        private void Start()
        {
            _spawners = new Dictionary<Type, IViewSpawner>();

            foreach (var viewSpawner in gameObject.GetComponentsInChildren<IViewSpawner>())
            {
                _spawners.Add(viewSpawner.GetType(), viewSpawner);
            }
        }

        public T GetSpawner<T>() where T : IViewSpawner
        {
            return (T) _spawners[typeof(T)];
        }
    }
}