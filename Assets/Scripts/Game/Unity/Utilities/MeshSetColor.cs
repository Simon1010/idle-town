using System;
using UnityEngine;

namespace Game.Unity.Utilities
{
    [ExecuteInEditMode]
    public class MeshSetColor : MonoBehaviour
    {
        public MeshRenderer Renderer;
        public Color color;

        private void Awake()
        {
            if (Renderer == null)
            {
                Renderer = GetComponent<MeshRenderer>();

                if (Renderer == null)
                {
                    Renderer = GetComponentInChildren<MeshRenderer>();
                }
            }
        }

        private void Update()
        {
            Renderer.material.color = color;
        }
    }
}