using System;
using UnityEngine;

namespace Utilities
{
    public class AutoAssignCamera : MonoBehaviour
    {
        private Canvas _canvas;
        private bool _isCanvasNotNull;
        private bool _isworldCameraNull;

        private void Start()
        {
            _isworldCameraNull = _canvas.worldCamera == null;
            _isCanvasNotNull = _canvas != null;
        }

        private void Awake()
        {
            _canvas = GetComponent<Canvas>();
        }

        private void Update()
        {
            if (_isCanvasNotNull && _isworldCameraNull)
            {
                _canvas.worldCamera = Camera.main;
            }
        }
    }
}