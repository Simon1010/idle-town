using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Serialization;
using Random = UnityEngine.Random;

namespace Utilities
{
    public class Shake : MonoBehaviour
    {
        public float _shakeDecay = 0.005f;

        public float shake_intensity;

        private void Awake()
        {
            TargetTransform = transform;
        }

        Vector3 originPosition;
        private Transform TargetTransform;

        IEnumerator ShakeIt()
        {
            while (shake_intensity > 0f)
            {
                TargetTransform.localPosition = originPosition + Random.insideUnitSphere * shake_intensity;
                shake_intensity -= _shakeDecay;
                yield return null;
            }

            ShakingStopped();

            yield return null;
        }

        void ShakingStopped()
        {
            TargetTransform.localPosition = originPosition;
        }

        public void DoShake(float strength, float decay)
        {
            originPosition = Vector3.zero;

            shake_intensity = strength;
            _shakeDecay = decay;
            StartCoroutine(nameof(ShakeIt));
        }
    }
}