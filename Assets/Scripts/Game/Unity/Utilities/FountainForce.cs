using System;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Utilities
{
    public class FountainForce : MonoBehaviour
    {
        public Vector2 ImpulseX = new Vector2(300f, 600f);
        public Vector2 ImpulseY = new Vector2(2f, 7f);
        private float _impulseX;
        private float _impulseY;

        private Rigidbody2D _rigidbody2D;

        private void Awake()
        {
            _rigidbody2D = GetComponent<Rigidbody2D>();
            
            _impulseX = Random.Range(ImpulseX.x, ImpulseX.y);

            if (Random.Range(0, 2) == 1)
            {
                _impulseX = -_impulseX;
            }

            _impulseY = Random.Range(ImpulseY.x, ImpulseY.y);

            _rigidbody2D.AddForce(new Vector2(_impulseX, _impulseY), ForceMode2D.Impulse);
        }

        private void Update()
        {
            transform.rotation.SetLookRotation(_rigidbody2D.velocity);
        }
    }
}