using System;
using UnityEngine;

namespace Utilities
{
    public class DestroyAfter : MonoBehaviour
    {
        public float Time;
        private bool _initialized;

        public void Initialize(float time)
        {
            Time = time;
        }

        private void Update()
        {
            Time -= UnityEngine.Time.deltaTime;
            if (Time <= 0)
            {
                GameObject.Destroy(gameObject);
            }
        }
    }
}