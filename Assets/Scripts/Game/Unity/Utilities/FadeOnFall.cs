using TMPro;
using UnityEngine;

namespace Utilities
{
    public class FadeOnFall : MonoBehaviour
    {
        public float FallTime = 0.25f;
        public float FadeDecay = 0.3f;
        public SpriteRenderer SpriteRenderer;
        public TMP_Text Text;
        public Rigidbody2D Rigidbody2D;
        public bool DestroyWhenDone = false;

        private void Update()
        {
            if (Rigidbody2D.velocity.y < 0)
            {
                FallTime -= Time.deltaTime;
                if (FallTime < 0)
                {
                    if (SpriteRenderer != null)
                    {
                        var color = SpriteRenderer.color;
                        color.a -= FadeDecay;
                        SpriteRenderer.color = color;

                        if (color.a <= 0)
                        {
                            if (DestroyWhenDone)
                            {
                                GameObject.Destroy(gameObject);
                            }
                        }
                    }

                    if (Text != null)
                    {
                        var color = Text.color;
                        color.a -= FadeDecay;
                        Text.color = color;

                        if (color.a <= 0)
                        {
                            if (DestroyWhenDone)
                            {
                                GameObject.Destroy(gameObject);
                            }
                        }
                    }
                }
            }
        }
    }
}