﻿using System;
using DigitalRuby.Tween;
using Game.Unity;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public ICameraFocuseable Target;
    public float angleQuadrant = -90f;
    public Vector3 StartPosition;
    public float RotationSpeed = 15f;
    public float ZoomSpeed = 15f;
    public float OrbitTweenTime = 0.3f;
    public float StartQuadrant = -40f;
    private int _currentQuadrant = 0;


    private bool _isMouseDown = false;
    private Vector3 _mousePreviousPosition;
    private Vector3 _mouseNowPosition;

    private Action<ITween<float>> _rotateCamTween;
    private FloatTween _currentTween;

    private void Awake()
    {
        StartPosition = transform.position;
        _rotateCamTween = t => { SetCurrentRotation(t.CurrentValue); };
    }


    void Update()
    {
        if (Target == null)
        {
            if (_currentTween == null || Mathf.FloorToInt(_currentTween.CurrentProgress) == 1)
            {
                SetCurrentRotation(transform.parent.eulerAngles.y - 15f * Time.deltaTime);
            }

            return;
        }


        if (Input.GetKeyDown(KeyCode.A))
        {
            ShiftRotationQuadrant(-1);
        }

        if (Input.GetKeyDown(KeyCode.D))
        {
            ShiftRotationQuadrant(1);
        }

        if (Input.GetMouseButtonDown(0))
        {
            _isMouseDown = true;
            _mousePreviousPosition = Input.mousePosition;
            _mouseNowPosition = Input.mousePosition;
        }

        if (_isMouseDown)
        {
            if (Input.GetMouseButton(0))
            {
                _mouseNowPosition = Input.mousePosition;

                var distance = (_mouseNowPosition.x - _mousePreviousPosition.x) * RotationSpeed * Time.deltaTime;
//                SetCurrentRotationTween(target.eulerAngles.y , target.eulerAngles.y + distance);
                SetCurrentRotation(transform.parent.eulerAngles.y + distance);
                _mousePreviousPosition = Input.mousePosition;
            }

            if (Input.GetMouseButtonUp(0))
            {
                _isMouseDown = false;
            }
        }
        else
        {
            if (_currentTween == null || Mathf.FloorToInt(_currentTween.CurrentProgress) == 1)
            {
                SetCurrentRotation(transform.parent.eulerAngles.y - 15f * Time.deltaTime);
            }
        }


        transform.LookAt(Target.Position);
        HandleZoom();
    }

    private void HandleZoom()
    {
        var mouseWheel = Input.mouseScrollDelta.y * ZoomSpeed * Time.deltaTime;

        var distance = Vector3.Distance(transform.position, Target.Position);
        if (mouseWheel < 0 && distance > Target.GetMaxZoom())
        {
            mouseWheel = 0;
        }
        else if (mouseWheel > 0 && distance < Target.GetMinZoom())
        {
            mouseWheel = 0;
        }

        SetZoom(mouseWheel);
    }

    private void SetZoom(float currentZoom)
    {
        var pos = transform.position;
        pos += transform.forward * currentZoom;
        transform.position = pos;
    }


    public void RefreshZoom()
    {
        if (Target == null)
        {
            return;
        }


        var distance = Vector3.Distance(transform.position, Target.Position);
        var zoomSpeed = .5f;
        while (distance > Target.GetMaxZoom())
        {
            SetZoom(zoomSpeed * Time.deltaTime);
            distance = Vector3.Distance(transform.position, Target.Position);
        }

        distance = Vector3.Distance(transform.position, Target.Position);
        while (distance < Target.GetMinZoom())
        {
            SetZoom(-zoomSpeed * Time.deltaTime);
            distance = Vector3.Distance(transform.position, Target.Position);
        }
    }


    public void SetRotationQuadrant(int targetQuadrant)
    {
        var currentQuadrant = _currentQuadrant;

        if (targetQuadrant == -1)
        {
            targetQuadrant = 3;
        }
        else
        {
            if (targetQuadrant == 4)
            {
                targetQuadrant = 0;
            }
        }

        if (currentQuadrant == 3 && targetQuadrant == 0)
        {
            currentQuadrant = -1;
        }


        if (currentQuadrant == 0 && targetQuadrant == 3)
        {
            currentQuadrant = 4;
        }


        var currentRot = StartQuadrant + angleQuadrant * currentQuadrant;
        var newRot = StartQuadrant + angleQuadrant * targetQuadrant;

        SetCurrentRotationTween(currentRot, newRot);
        _currentQuadrant = targetQuadrant;
    }

    private void SetCurrentRotationTween(float currentRot, float newRot)
    {
        _currentTween = TweenFactory.Tween("CameraRotation", currentRot, newRot, OrbitTweenTime,
            TweenScaleFunctions.Linear,
            _rotateCamTween);
    }

    private void SetCurrentRotation(float yRotation)
    {
        transform.parent.eulerAngles = new Vector3(0, yRotation, 0);
    }

    public void ShiftRotationQuadrant(int id)
    {
        var newQuadrant = _currentQuadrant + id;
        SetRotationQuadrant(newQuadrant);
    }
}