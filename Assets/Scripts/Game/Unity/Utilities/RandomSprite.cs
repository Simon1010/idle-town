using UnityEngine;
using Random = UnityEngine.Random;

namespace Utilities
{
    public class RandomSprite : MonoBehaviour
    {
        public Sprite[] Sprites;
        public SpriteRenderer Renderer;

        private void Awake()
        {
            Renderer = GetComponent<SpriteRenderer>();
            Renderer.sprite = Sprites[Random.Range(0, Sprites.Length)];
        }
    }
}