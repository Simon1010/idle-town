namespace Game
{
    public static class GameEnums
    {
        public enum GameState
        {
            Menu = 0,
            MainMenu = 1,
            Gameplay = 2,
        }

        public enum MenuId
        {
            Main = 0,
            Settings = 1,
            WorkerManagement = 2,
            ProductManagement = 3,
        }

        public enum BuildingId
        {
            ShedSmall = 0,
            BarnSmall = 1,
            FoodCart = 2,
        }

        public enum UpgradeId
        {
            Default = 0,
            HqBuildingResearch = 1,
            HqTotalWorkers = 2,
            ConstructionJobCraneSmall = 50,
            ConstructionJobCraneMedium = 51,
            ConstructionJobCraneLarge = 52,
            ConstructionJobWorkerFood = 53,
            ConstructionJobGeneratorSmall = 54,
            ConstructionJobGeneratorMedium = 55,
            ConstructionJobGeneratorLarge = 56,
            BuildingAcUnitSmall = 100,
            BuildingAcUnitMedium = 101,
            BuildingAcUnitLarge = 102,
            BuildingSecuritySystem = 103,
            BuildingCleaners = 104,
            BuildingGardenSmall = 105,
            BuildingGardenMedium = 106,
            BuildingGardenLarge = 107,
            BuildingWindmillSmall = 108,
            BuildingWindmillLarge = 109,
        }

        public enum StatId
        {
            Speed = 0,
            TotalWorkers = 1,
            BuildingResearch = 2,
        }

        public enum UpgradeCategory
        {
            Building = 0,
            ConstructionJob = 1,
            Headquarters = 2,
        }
    }
}