using System.Linq;
using Game.Data.Requirements;

namespace Game.Data
{
    public class UpgradeData
    {
        public GameEnums.UpgradeId UpgradeId { get; set; }
        public GameEnums.UpgradeCategory UpgradeCategory { get; set; }

        public int MaxLevel;

        public RequirementCollection Requirements;
        public StatCollection Stats;
        public ShopDataCollection ShopDataCollection;

        public UpgradeData()
        {
            Stats = new StatCollection();
            Requirements = new RequirementCollection();
        }

        public bool IsValid(Contexts contexts, int level)
        {
            return Requirements.GetRequirements(level).All(e => e.IsValid(contexts));
        }

        public ShopData GetShopDataByLevel(int level)
        {
            return ShopDataCollection.GetShopData(level);
        }

        public StatData[] GetStatsByLevel(int level)
        {
            return Stats.GetStats(level);
        }

        public float GetCost(int level)
        {
            return GameCalculations.CalculateStat(GetShopDataByLevel(level).SellPrice, level);
        }
    }
}