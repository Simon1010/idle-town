using System.Linq;

namespace Game.Data.Requirements
{
    public class RequirementPreviousUpgrade : IRequirement
    {
        private readonly GameEnums.UpgradeId _previousUpgradeId;
        private int _level;

        public RequirementPreviousUpgrade(GameEnums.UpgradeId previousUpgradeId, int level = 1)
        {
            _previousUpgradeId = previousUpgradeId;
            _level = level;
        }

        public bool IsValid(Contexts contexts)
        {
            var upgrade = contexts.core.GetEntitiesWithUpgradeId(_previousUpgradeId).Single();

            if (upgrade == null)
            {
                return false;
            }

            return upgrade.upgradeLevel.value >= _level;
        }
    }
}