namespace Game.Data.Requirements
{
    public interface IRequirement
    {
        bool IsValid(Contexts contexts);
    }
}