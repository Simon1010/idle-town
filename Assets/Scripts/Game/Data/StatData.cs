namespace Game.Data
{
    public class StatData
    {
        public float Base;
        public float Multiplier;
        public GameEnums.StatId StatId;

        public bool IsAggregateValue = true;
    }
}