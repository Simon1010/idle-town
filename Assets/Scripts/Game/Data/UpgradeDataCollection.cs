using System.Collections.Generic;

namespace Game.Data
{
    public class UpgradeDataCollection 
    {
        public Dictionary<int, UpgradeData> UpgradeData;

        public UpgradeData GetShopData(int level)
        {
            var highestLevel = -1;
            foreach (var data in UpgradeData)
            {
                if (data.Key <= level && highestLevel < data.Key)
                {
                    highestLevel = data.Key;
                }
            }

            return UpgradeData[highestLevel];
        }
    }
}