using System.Collections.Generic;

namespace Game.Data
{
    public class ShopDataCollection 
    {
        public Dictionary<int, ShopData> ShopData;

        public ShopData GetShopData(int level)
        {
            var highestLevel = -1;
            foreach (var data in ShopData)
            {
                if (data.Key <= level && highestLevel < data.Key)
                {
                    highestLevel = data.Key;
                }
            }

            return ShopData[highestLevel];
        }
    }
}