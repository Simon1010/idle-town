using System.Collections.Generic;
using Game.Data.Requirements;

namespace Game.Data
{
    public class RequirementCollection
    {
        public Dictionary<int, IRequirement[]> Requirements;

        public RequirementCollection()
        {
            Requirements = new Dictionary<int, IRequirement[]>();
        }

        public IRequirement[] GetRequirements(int level)
        {
            if (Requirements.Count == 0)
            {
                return new IRequirement[0];
            }

            if (Requirements.ContainsKey(level) == false)
            {
                return new IRequirement[0];
            }

            return Requirements[level];
        }
    }
}