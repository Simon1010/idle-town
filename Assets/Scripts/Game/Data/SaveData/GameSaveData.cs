using System;

namespace Game.Data.SaveData
{
    [Serializable]
    public class GameSaveData
    {
        public DateTime GameOverDate;
        public float GameOverTimeLeft = 0f;
        public bool IsGameOver = false;
        public float BarricadeHealth = 0;
        public DateTime LastLoginDate;
        public int TotalKills = 0;
        public int PlayerMoney = 0;

        public int LastLoginGameDay = 0;
        public int LastLoginGameDayTime = 0;
        public long LastLoginGameTime = 0;
    }
}