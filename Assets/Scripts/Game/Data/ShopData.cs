namespace Game.Data
{
    public class ShopData 
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public StatData SellPrice { get; set; }
        public string IconFileName { get; set; }
    }
}