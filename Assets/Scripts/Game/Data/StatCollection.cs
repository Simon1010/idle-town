using System.Collections.Generic;

namespace Game.Data
{
    public class StatCollection
    {
        public Dictionary<int, StatData[]> Stats;

        public StatCollection()
        {
            Stats = new Dictionary<int, StatData[]>();
        }

        public StatData[] GetStats(int level)
        {
            if (Stats.Count == 0)
            {
                return new StatData[0];
            }

            var highestLevel = -1;
            foreach (var data in Stats)
            {
                if (data.Key <= level && highestLevel < data.Key)
                {
                    highestLevel = data.Key;
                }
            }

            return Stats[highestLevel];
        }
    }
}