using System.Collections.Generic;
using System.Linq;
using Game.Data;
using StatData = Game.Ecs.Core.General.Stat.StatData;

namespace Game
{
    public static class GameStaticData
    {
        public static StatData GetStatData(CoreEntity coreEntity, GameEnums.StatId statId)
        {
            return GetAllStatData(coreEntity).Single(e => e.StatId == statId);
        }

        public static StatData[] GetAllStatData(CoreEntity target)
        {
//            if (target.isBusiness)
//            {
//                if (target.businessId.value == GameEnums.BusinessId.IceCream)
//                {
//                    return new[]
//                    {
//                        new StatData
//                        {
//                            StatId = GameEnums.StatId.TotalWorkers,
//                            Base = 2,
//                            Multiplier = 0
//                        }
//                    };
//                }

            return new StatData[0];
        }


        public static UpgradeData GetUpgradeData(GameEnums.UpgradeId upgradeId)
        {
            return GetAllUpgradeData().Single(e => e.UpgradeId == upgradeId);
        }

        private static IEnumerable<UpgradeData> GetAllUpgradeData()
        {
            return new[]
            {
                CreateUpgradeData(GameEnums.UpgradeId.Default, 1)
            };
        }

        private static UpgradeData CreateUpgradeData(GameEnums.UpgradeId upgradeId, int maxLevel,
            RequirementCollection requirements = null, StatCollection stats = null,
            ShopDataCollection shopDataCollection = null)
        {
            return new UpgradeData()
            {
                Requirements = requirements,
                Stats = stats,
                MaxLevel = maxLevel,
                UpgradeId = upgradeId,
                ShopDataCollection = shopDataCollection,
            };
        }
    }
}