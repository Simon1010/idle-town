using UnityEngine;
using Vector2 = System.Numerics.Vector2;
using Vector3 = System.Numerics.Vector3;

namespace Game.Ecs
{
    public static class GameFactory
    {
        public static void CreateCollision(Contexts contexts, CoreEntityId entityA, CoreEntityId entityB)
        {
            var collision = contexts.core.CreateEntity();
            collision.AddCollision(entityA, entityB);
        }

        public static CoreEntity CreateTick(Contexts contexts, float total, float start = 0)
        {
            var tick = contexts.core.CreateEntity();
            tick.isTick = true;
            tick.AddTickCurrentTime(start);
            tick.AddTickTotalTime(total);
            return tick;
        }

        public static void CreateTap(Contexts contexts, Vector3 position)
        {
            var entity = contexts.core.CreateEntity();
            entity.isTap = true;
            entity.AddPosition(position);
        }

        public static CoreEntity CreateSwipe(Contexts contexts, Vector2 direction)
        {
            var entity = contexts.core.CreateEntity();
            entity.isSwipe = true;
            entity.AddSwipeDirection(direction);
            return entity;
        }

        public static CoreEntity CreatePlayer(Contexts contexts, Vector3 position)
        {
            var entity = contexts.core.CreateEntity();
            entity.isPlayer = true;
            entity.AddPosition(position);
            entity.AddPlayerMoney(300);
            return entity;
        }

        public static CoreEntity CreateBuilding(Contexts contexts, GameEnums.BuildingId buildingId, CoreEntityId plotId)
        {
            var entity = contexts.core.CreateEntity();
            entity.isBuilding = true;
            entity.AddBuildingId(buildingId);
            entity.AddBuildingPlot(plotId);
            return entity;
        }

        public static CoreEntity CreatePlot(Contexts contexts, Vector3 gridPosition, CoreEntityId mapId)
        {
            var entity = contexts.core.CreateEntity();
            entity.isPlot = true;
            entity.AddPlotGridPosition(gridPosition);
            entity.AddPlotMap(mapId);
            return entity;
        }


        public static CoreEntity CreateGameMeta(Contexts contexts)
        {
            var entity = contexts.core.CreateEntity();
            entity.isGameMeta = true;
            return entity;
        }


        public static CoreEntity CreateUpgrade(Contexts contexts, GameEnums.UpgradeId upgradeId,
            CoreEntityId targetId, int startingLevel = 0)
        {
            var entity = contexts.core.CreateEntity();
            entity.isUpgrade = true;
            entity.AddUpgradeTargetId(targetId);

            var upgradeData = GameStaticData.GetUpgradeData(upgradeId);
            entity.AddUpgradeId(upgradeId);
            entity.AddUpgradeLevel(startingLevel);
            entity.AddUpgradeMaxLevel(upgradeData.MaxLevel);
            entity.AddUpgradeCategory(upgradeData.UpgradeCategory);

            return entity;
        }

        public static CoreEntity CreateMap(Contexts contexts)
        {
            var entity = contexts.core.CreateEntity();
            entity.isMap = true;
            entity.AddMapSize(Vector3.Zero);
            return entity;
        }
    }
}