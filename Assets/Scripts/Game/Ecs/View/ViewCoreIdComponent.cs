using Entitas;

namespace Game.Ecs.View
{
    [View]
    public class ViewCoreIdComponent : IComponent
    {
        public CoreEntityId value;
    }
}