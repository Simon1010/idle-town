namespace Game.Ecs.View
{
    public interface IViewSpawner : IContextUser
    {
        void Initialize(IViewSpawner[] otherSpawners);
    }
}