using System;
using System.Linq;
using Entitas;
using Game.Ecs.Commands;
using Game.Ecs.Core;
using Game.Ecs.View;
using UnityEngine;
using ZombieGame;
using Random = UnityEngine.Random;
using Vector3 = System.Numerics.Vector3;

namespace Game.Ecs
{
    public class EcsController
    {
        public Contexts Contexts
        {
            get => _contexts;
        }

        private Contexts _contexts;
        private Systems _systems;

        public EcsController()
        {
            _contexts = Contexts.sharedInstance;
            _contexts.SubscribeId();

            _systems = new Feature()
                    .Add(new InputActionFeature(_contexts))
                    .Add(new CommandFeature(_contexts))
                    .Add(new CoreFeature(_contexts))
                    .Add(new ViewFeature(_contexts))
                ;
        }

        public void Initialize()
        {
            _systems.Initialize();

            StartGame();
        }


        public void Update()
        {
            _systems.Execute();
            _systems.Cleanup();

            if (Input.GetKeyDown(KeyCode.K))
            {
                var plots = _contexts.core.GetEntities(CoreMatcher.Plot).Where(e =>
                    _contexts.core.GetEntities(CoreMatcher.BuildingPlot)
                        .Any(b => b.buildingPlot.value == e.coreId.value) == false).ToArray();


                var buildingIdValues = Enum.GetValues(typeof(GameEnums.BuildingId));
                GameFactory.CreateBuilding(_contexts,
                    (GameEnums.BuildingId) Random.Range(0, buildingIdValues.Length),
                    plots[Random.Range(0, plots.Length)].coreId.value);
            }
        }

        private void StartGame()
        {
            GameSoundManager.Instance.Play("music_loop", true);

            var gameplayTick = GameFactory.CreateTick(_contexts, 0.001f);
            gameplayTick.isGameplayTick = true;

            var map = GameFactory.CreateMap(_contexts);

            var player = GameFactory.CreatePlayer(_contexts, Vector3.Zero);
            player.AddPlayerMap(map.coreId.value);

            for (int xIndex = 0; xIndex < 3; xIndex++)
            {
                for (int zIndex = 0; zIndex < 3; zIndex++)
                {
                    GameFactory.CreatePlot(_contexts, new Vector3(xIndex, 0, zIndex), map.coreId.value);
                }
            }
        }

        public void TearDown()
        {
            _systems.TearDown();
        }
    }
}