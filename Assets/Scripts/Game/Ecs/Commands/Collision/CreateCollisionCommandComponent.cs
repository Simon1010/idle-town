using Entitas;

namespace Game.Ecs.Commands.Collision
{
    [Command]
    public class CreateCollisionCommandComponent : IComponent
    {
        public CoreEntityId entityA;
        public CoreEntityId entityB;
    }
}