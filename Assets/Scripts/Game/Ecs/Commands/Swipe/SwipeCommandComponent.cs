using System.Numerics;
using Entitas;

namespace Game.Ecs.Commands.Swipe
{
    [Command]
    public class SwipeCommandComponent : IComponent
    {
        public Vector2 direction;
    }
}