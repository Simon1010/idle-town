using System.Collections.Generic;
using Entitas;

namespace Game.Ecs.Commands.Swipe
{
    public class SwipeInputActionSystem : ReactiveSystem<InputActionEntity>
    {
        private readonly Contexts _contexts;

        public SwipeInputActionSystem(Contexts contexts) : base(contexts.inputAction)
        {
            _contexts = contexts;
        }

        protected override ICollector<InputActionEntity> GetTrigger(IContext<InputActionEntity> context)
        {
            return context.CreateCollector(InputActionMatcher.SwipeInputAction);
        }

        protected override bool Filter(InputActionEntity entity)
        {
            return entity.hasSwipeInputAction;
        }

        protected override void Execute(List<InputActionEntity> entities)
        {
            foreach (var inputActionEntity in entities)
            {
                GameCommands.CreateSwipe(_contexts, inputActionEntity.swipeInputAction.direction);
            }
        }
    }
}