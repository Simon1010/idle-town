using System.Numerics;
using Entitas;

namespace Game.Ecs.Commands.Swipe
{
    [InputAction]
    public class SwipeInputActionComponent : IComponent
    {
        public Vector2 direction;
    }
}