using System.Collections.Generic;
using Entitas;

namespace Game.Ecs.Commands.Swipe
{
    public class SwipeCommandSystem : ReactiveSystem<CommandEntity>
    {
        private readonly Contexts _contexts;

        public SwipeCommandSystem(Contexts contexts) : base(contexts.command)
        {
            _contexts = contexts;
        }

        protected override ICollector<CommandEntity> GetTrigger(IContext<CommandEntity> context)
        {
            return context.CreateCollector(CommandMatcher.SwipeCommand);
        }

        protected override bool Filter(CommandEntity entity)
        {
            return entity.hasSwipeCommand;
        }

        protected override void Execute(List<CommandEntity> entities)
        {
            foreach (var commandEntity in entities)
            {
                GameFactory.CreateSwipe(_contexts, commandEntity.swipeCommand.direction);
            }
        }
    }
}