using System.Collections.Generic;
using System.Numerics;
using Entitas;

namespace Game.Ecs.Commands.Tap
{
    public class TapCommandSystem : ReactiveSystem<CommandEntity>
    {
        private readonly Contexts _contexts;

        public TapCommandSystem(Contexts contexts) : base(contexts.command)
        {
            _contexts = contexts;
        }

        protected override ICollector<CommandEntity> GetTrigger(IContext<CommandEntity> context)
        {
            return context.CreateCollector(CommandMatcher.TapCommand);
        }

        protected override bool Filter(CommandEntity entity)
        {
            return entity.hasTapCommand;
        }

        protected override void Execute(List<CommandEntity> entities)
        {
            foreach (var commandEntity in entities)
            {
                var pos = new Vector3();
                pos.X = commandEntity.tapCommand.position.X;
                pos.Y = commandEntity.tapCommand.position.Y;
                GameFactory.CreateTap(_contexts, pos);
            }
        }
    }
}