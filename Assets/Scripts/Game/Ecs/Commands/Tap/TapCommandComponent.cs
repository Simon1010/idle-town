using System.Numerics;
using Entitas;

namespace Game.Ecs.Commands.Tap
{
    [Command]
    public class TapCommandComponent : IComponent
    {
        public Vector2 position;
    }
}