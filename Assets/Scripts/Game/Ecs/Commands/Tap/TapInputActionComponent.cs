using System.Numerics;
using Entitas;

namespace Game.Ecs.Commands.Tap
{
    [InputAction]
    public class TapInputActionComponent : IComponent
    {
        public Vector2 position;
    }
}