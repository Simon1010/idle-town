using Game.Ecs.Commands.Collision;
using Game.Ecs.Commands.SetGameState;
using Game.Ecs.Commands.Swipe;
using Game.Ecs.Commands.Tap;

namespace Game.Ecs.Commands
{
    public class InputActionFeature : Feature
    {
        public InputActionFeature(Contexts contexts)
        {
            Add(new CreateCollisionInputActionSystem(contexts));
            Add(new SetGameStateInputActionSystem(contexts));
            Add(new TapInputActionSystem(contexts));
            Add(new SwipeInputActionSystem(contexts));
            Add(new DestroyInputActionsSystem(contexts));
        }
    }
}