using System;
using System.Numerics;

namespace Game.Ecs.Commands
{
    public static class GameInputActions
    {
        private static InputActionEntity CreateInputAction(Contexts contexts)
        {
            var inputAction = contexts.inputAction.CreateEntity();
            inputAction.isInputAction = true;
            return inputAction;
        }

        public static void CreateInputActionCollision(Contexts contexts, CoreEntityId entityA, CoreEntityId entityB)
        {
            var inputAction = CreateInputAction(contexts);
            inputAction.AddCreateCollisionInputAction(entityA, entityB);
        }


        public static void CreateSetGameState(Contexts contexts, GameEnums.GameState state)
        {
            var inputAction = CreateInputAction(contexts);
            inputAction.AddSetGameStateInputAction(state);
        }

        public static void CreateTap(Contexts contexts, Vector2 position)
        {
            var inputAction = CreateInputAction(contexts);
            inputAction.AddTapInputAction(position);
        }


        public static void CreateSwipe(Contexts contexts, Vector2 direction)
        {
            var inputAction = CreateInputAction(contexts);
            inputAction.AddSwipeInputAction(direction);
        }
    }
}