using Game.Ecs.Commands.Collision;
using Game.Ecs.Commands.SetGameState;
using Game.Ecs.Commands.Swipe;
using Game.Ecs.Commands.Tap;

namespace Game.Ecs.Commands
{
    public class CommandFeature : Feature
    {
        public CommandFeature(Contexts contexts)
        {
            Add(new CreateCollisionCommandSystem(contexts));
            Add(new SetGameStateCommandSystem(contexts));
            Add(new TapCommandSystem(contexts));
            Add(new SwipeCommandSystem(contexts));

            Add(new DestroyCommandsSystem(contexts));
        }
    }
}