using Entitas;

namespace Game.Ecs.Commands.SetGameState
{
    [InputAction]
    public class SetGameStateInputActionComponent : IComponent
    {
        public GameEnums.GameState GameState;
    }
}