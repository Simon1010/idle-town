using Entitas;

namespace Game.Ecs.Commands.SetGameState
{
    [Command]
    public class SetGameStateCommandComponent : IComponent
    {
        public GameEnums.GameState GameState;
    }
}