using Game.Ecs.Core.Map.MapSize;
using Game.Ecs.Core.Map.Plot;

namespace Game.Ecs.Core.Map
{
    public class MapFeature : Feature
    {
        public MapFeature(Contexts contexts)
        {
            Add(new PlotFeature(contexts));
            Add(new MapSizeFeature(contexts));
        }
    }
}