namespace Game.Ecs.Core.Map.MapSize
{
    public class MapSizeFeature : Feature
    {
        public MapSizeFeature(Contexts contexts)
        {
            Add(new CalculateMapSizeSystem(contexts));
        }
    }
}