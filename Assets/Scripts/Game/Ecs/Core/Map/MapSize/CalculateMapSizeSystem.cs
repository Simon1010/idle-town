using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using Entitas;

namespace Game.Ecs.Core.Map.MapSize
{
    public class CalculateMapSizeSystem : ReactiveSystem<CoreEntity>
    {
        private readonly Contexts _contexts;

        public CalculateMapSizeSystem(Contexts contexts) : base(contexts.core)
        {
            _contexts = contexts;
        }

        protected override ICollector<CoreEntity> GetTrigger(IContext<CoreEntity> context)
        {
            return context.CreateCollector(CoreMatcher.Plot.Added(), CoreMatcher.Destroyed.Added());
        }

        protected override bool Filter(CoreEntity entity)
        {
            return entity.hasPlotMap;
        }

        protected override void Execute(List<CoreEntity> entities)
        {
            foreach (var coreEntity in entities)
            {
                var plots = _contexts.core.GetEntitiesWithPlotMap(coreEntity.plotMap.value)
                    .Where(e => e.isDestroyed == false);

                var totalColumns = plots.Max(e => e.plotGridPosition.value.X);
                var totalRows = plots.Max(e => e.plotGridPosition.value.Z);
                var map = _contexts.core.GetEntityWithCoreId(coreEntity.plotMap.value);

                map.ReplaceMapSize(new Vector3(totalColumns + 1, 0, totalRows + 1));
            }
        }
    }
}