using System.Numerics;
using Entitas;

namespace Game.Ecs.Core.Map
{
    [Core]
    public class MapSizeComponent : IComponent
    {
        public Vector3 value;
    }
}