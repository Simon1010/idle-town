using Game.Ecs.Core.Map.Plot.Building;
using Game.Ecs.Core.Map.Plot.PositionPlot;

namespace Game.Ecs.Core.Map.Plot
{
    public class PlotFeature : Feature
    {
        public PlotFeature(Contexts contexts)
        {
            Add(new PositionPlotSystem(contexts));
            Add(new BuildingFeature(contexts));
        }
    }
}