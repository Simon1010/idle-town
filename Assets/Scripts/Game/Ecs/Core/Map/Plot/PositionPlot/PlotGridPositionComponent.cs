using System.Numerics;
using Entitas;

namespace Game.Ecs.Core.Map.Plot.PositionPlot
{
    [Core]
    public class PlotGridPositionComponent : IComponent
    {
        public Vector3 value;
    }
}