using System.Collections.Generic;
using Entitas;

namespace Game.Ecs.Core.Map.Plot.PositionPlot
{
    public class PositionPlotSystem : ReactiveSystem<CoreEntity>
    {
        private readonly Contexts _contexts;

        public PositionPlotSystem(Contexts contexts) : base(contexts.core)
        {
            _contexts = contexts;
        }

        protected override ICollector<CoreEntity> GetTrigger(IContext<CoreEntity> context)
        {
            return context.CreateCollector(CoreMatcher.PlotGridPosition);
        }

        protected override bool Filter(CoreEntity entity)
        {
            return entity.hasPlotGridPosition;
        }

        protected override void Execute(List<CoreEntity> entities)
        {
            foreach (var coreEntity in entities)
            {
                var position = coreEntity.plotGridPosition.value * (GameSettings.PlotSize * 2);
                coreEntity.ReplacePosition(position);
            }
        }
    }
}