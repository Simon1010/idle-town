namespace Game.Ecs.Core.Map.Plot.Building
{
    public class BuildingFeature : Feature
    {
        public BuildingFeature(Contexts contexts)
        {
            Add(new PositionBuildingSystem(contexts));
        }
    }
}