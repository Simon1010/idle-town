using Entitas;

namespace Game.Ecs.Core.Map.Plot.Building
{
    [Core]
    public class BuildingIdComponent : IComponent
    {
        public GameEnums.BuildingId value;
    }
}