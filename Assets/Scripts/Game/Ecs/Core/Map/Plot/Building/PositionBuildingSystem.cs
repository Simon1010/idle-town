using System.Collections.Generic;
using Entitas;

namespace Game.Ecs.Core.Map.Plot.Building
{
    public class PositionBuildingSystem : ReactiveSystem<CoreEntity>
    {
        private readonly Contexts _contexts;

        public PositionBuildingSystem(Contexts contexts) : base(contexts.core)
        {
            _contexts = contexts;
        }

        protected override ICollector<CoreEntity> GetTrigger(IContext<CoreEntity> context)
        {
            return context.CreateCollector(CoreMatcher.BuildingPlot);
        }

        protected override bool Filter(CoreEntity entity)
        {
            return entity.hasBuildingPlot;
        }

        protected override void Execute(List<CoreEntity> entities)
        {
            foreach (var coreEntity in entities)
            {
                var plot = _contexts.core.GetEntityWithCoreId(coreEntity.buildingPlot.value);
                coreEntity.ReplacePosition(plot.position.value);
            }
        }
    }
}