using Entitas;
using Entitas.CodeGeneration.Attributes;

namespace Game.Ecs.Core.Map.Plot
{
    [Core]
    public class PlotMapComponent : IComponent
    {
        [EntityIndex] public CoreEntityId value;
    }
}