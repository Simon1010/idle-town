using System.Collections.Generic;
using Entitas;

namespace Game.Ecs.Core.Player.FocusPosition
{
    public class FocusOnPlotSystem : ReactiveSystem<CoreEntity>
    {
        private readonly Contexts _contexts;

        public FocusOnPlotSystem(Contexts contexts) : base(contexts.core)
        {
            _contexts = contexts;
        }

        protected override ICollector<CoreEntity> GetTrigger(IContext<CoreEntity> context)
        {
            return context.CreateCollector(CoreMatcher.PlayerCurrentPlot);
        }

        protected override bool Filter(CoreEntity entity)
        {
            return entity.hasPlayerCurrentPlot;
        }

        protected override void Execute(List<CoreEntity> entities)
        {
            foreach (var coreEntity in entities)
            {
                var plot = _contexts.core.GetEntityWithCoreId(coreEntity.playerCurrentPlot.value);
                coreEntity.ReplacePosition(plot.position.value);
            }
        }
    }
}