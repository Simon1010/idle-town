using Entitas;
using IdleMineGenerator.CodeGeneration.Attributes;

namespace Game.Ecs.Core.Player.FocusPosition
{
    [Core, ListenerSelf]
    public class PlayerCurrentPlotComponent : IComponent
    {
        public CoreEntityId value;
    }
}