using System.Collections.Generic;
using System.Numerics;
using Entitas;
using UnityEngine;

namespace Game.Ecs.Core.Player.FocusPosition
{
    public class FocusOnMapSystem : ReactiveSystem<CoreEntity>
    {
        private readonly Contexts _contexts;

        public FocusOnMapSystem(Contexts contexts) : base(contexts.core)
        {
            _contexts = contexts;
        }

        protected override ICollector<CoreEntity> GetTrigger(IContext<CoreEntity> context)
        {
            return context.CreateCollector(CoreMatcher.Player.Added(), CoreMatcher.PlayerCurrentPlot.Removed(),
                CoreMatcher.MapSize.Added());
        }

        protected override bool Filter(CoreEntity entity)
        {
            return entity.isPlayer && entity.hasPlayerCurrentPlot == false || entity.isMap;
        }

        protected override void Execute(List<CoreEntity> entities)
        {
            CoreEntity map = null;

            foreach (var coreEntity in entities)
            {
                if (coreEntity.isMap)
                {
                    map = coreEntity;
                }
                else if (coreEntity.isPlayer)
                {
                    map = _contexts.core.GetEntityWithCoreId(coreEntity.playerMap.value);
                }

                var player = _contexts.core.playerEntity;
                var mapSize = map.mapSize.value;

                var columnCenter = (int) mapSize.X / 2;
                var rowCenter = (int) mapSize.Z / 2;

                var mapGlobalCenter = new System.Numerics.Vector3
                {
                    X = columnCenter * GameSettings.PlotSize.X * 2, Z = rowCenter * GameSettings.PlotSize.Z * 2
                };
                player.ReplacePosition(mapGlobalCenter);
            }
        }
    }
}