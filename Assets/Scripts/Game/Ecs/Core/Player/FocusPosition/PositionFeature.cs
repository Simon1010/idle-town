namespace Game.Ecs.Core.Player.FocusPosition
{
    public class PositionFeature : Feature
    {
        public PositionFeature(Contexts contexts)
        {
            Add(new FocusOnPlotSystem(contexts));
            Add(new FocusOnMapSystem(contexts));
        }
    }
}