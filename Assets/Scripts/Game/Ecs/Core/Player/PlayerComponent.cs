using Entitas;
using Entitas.CodeGeneration.Attributes;
using IdleMineGenerator.CodeGeneration.Attributes;

namespace Game.Ecs.Core.Player
{
    [Core, Unique, ListenerAny]
    public class PlayerComponent : IComponent
    {
    }
}