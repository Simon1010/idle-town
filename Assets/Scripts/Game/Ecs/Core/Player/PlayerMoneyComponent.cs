using Entitas;
using IdleMineGenerator.CodeGeneration.Attributes;

namespace Game.Ecs.Core.Player
{
    [Core, ListenerSelf, ListenerAny]
    public class PlayerMoneyComponent : IComponent
    {
        public float value;
    }
}