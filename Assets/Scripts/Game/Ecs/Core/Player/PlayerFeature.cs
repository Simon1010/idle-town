using Game.Ecs.Core.Player.FocusPosition;

namespace Game.Ecs.Core.Player
{
    public class PlayerFeature : Feature
    {
        public PlayerFeature(Contexts contexts)
        {
            Add(new PositionFeature(contexts));
        }
    }
}