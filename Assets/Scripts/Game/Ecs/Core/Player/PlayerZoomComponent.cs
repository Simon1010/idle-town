using Entitas;
using IdleMineGenerator.CodeGeneration.Attributes;

namespace Game.Ecs.Core.Player
{
    [Core, ListenerSelf()]
    public class PlayerZoomComponent : IComponent
    {
        public float value;
    }
}