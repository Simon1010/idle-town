using Game.Ecs.Core.GameMeta;
using Game.Ecs.Core.General;
using Game.Ecs.Core.General.Destroy;
using Game.Ecs.Core.General.Tick;
using Game.Ecs.Core.Map;
using Game.Ecs.Core.Player;

namespace Game.Ecs.Core
{
    public class CoreFeature : Feature
    {
        public CoreFeature(Contexts contexts)
        {
            Add(new GameMetaFeature(contexts));

            Add(new TickFeature(contexts));
            Add(new PlayerFeature(contexts));
            Add(new MapFeature(contexts));


            Add(new GeneralFeature(contexts));

            Add(new AssignDestroyFeature(contexts));
            Add(new ListenersEventSystem(contexts));
            Add(new DestroyEntitiesSystem(contexts));
        }
    }
}