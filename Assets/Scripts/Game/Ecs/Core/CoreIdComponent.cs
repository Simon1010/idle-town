﻿using Entitas;
using Entitas.CodeGeneration.Attributes;

namespace Game.Ecs.Core
{
    [Core]
    public class CoreIdComponent : IComponent
    {
        [PrimaryEntityIndex] public CoreEntityId value;
    }
}
