using Entitas;

namespace Game.Ecs.Core.GameState
{
    [Core]
    public class CurrentStateComponent : IComponent
    {
        public GameEnums.GameState value;
    }
}