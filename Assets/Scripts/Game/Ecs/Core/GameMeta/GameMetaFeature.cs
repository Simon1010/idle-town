using Game.Ecs.Core.GameState;

namespace Game.Ecs.Core.GameMeta
{
    public class GameMetaFeature : Feature
    {
        public GameMetaFeature(Contexts contexts)
        {
            Add(new HandleGameStateChangeSystem(contexts));
        }
    }
}