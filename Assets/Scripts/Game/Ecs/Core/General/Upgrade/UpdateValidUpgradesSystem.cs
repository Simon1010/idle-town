using System.Collections.Generic;
using System.Linq;
using Entitas;

namespace Game.Ecs.Core.General.Upgrade
{
    public class UpdateValidUpgradesSystem : ReactiveSystem<CoreEntity>
    {
        private readonly Contexts _contexts;

        public UpdateValidUpgradesSystem(Contexts contexts) : base(contexts.core)
        {
            _contexts = contexts;
        }

        protected override ICollector<CoreEntity> GetTrigger(IContext<CoreEntity> context)
        {
            return context.CreateCollector(CoreMatcher.AllOf(CoreMatcher.GameplayTick, CoreMatcher.TickComplete));
        }

        protected override bool Filter(CoreEntity entity)
        {
            return entity.isGameplayTick && entity.isTickComplete;
        }

        protected override void Execute(List<CoreEntity> entities)
        {
            foreach (var coreEntity in entities)
            {
                var upgrades = _contexts.core.GetEntities(CoreMatcher.Upgrade);

                foreach (var upgrade in upgrades)
                {
                    var upgradeData = GameStaticData.GetUpgradeData(upgrade.upgradeId.value);
                    upgrade.isUpgradeValid = upgradeData.IsValid(_contexts, upgrade.upgradeLevel.value);
                }
            }
        }
    }
}