using Entitas;

namespace Game.Ecs.Core.General.Upgrade
{
    [Core]
    public class UpgradeCategoryComponent : IComponent
    {
        public GameEnums.UpgradeCategory value;
    }
}