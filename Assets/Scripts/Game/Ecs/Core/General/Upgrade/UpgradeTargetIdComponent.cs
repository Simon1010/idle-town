using Entitas;

namespace Game.Ecs.Core.General.Upgrade
{
    [Core]
    public class UpgradeTargetIdComponent : IComponent
    {
        public CoreEntityId value;
    }
}