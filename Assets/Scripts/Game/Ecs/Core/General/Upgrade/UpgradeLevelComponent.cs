using Entitas;
using IdleMineGenerator.CodeGeneration.Attributes;

namespace Game.Ecs.Core.General.Upgrade
{
    [Core, ListenerAny, ListenerSelf]
    public class UpgradeLevelComponent : IComponent
    {
        public int value;
    }
}