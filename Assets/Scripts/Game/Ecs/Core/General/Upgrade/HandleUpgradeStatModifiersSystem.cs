using System.Collections.Generic;
using System.Linq;
using Entitas;
using Game.Data;
using UnityEngine;

namespace Game.Ecs.Core.General.Upgrade
{
    public class HandleUpgradeStatModifiersSystem : ReactiveSystem<CoreEntity>
    {
        private readonly Contexts _contexts;

        public HandleUpgradeStatModifiersSystem(Contexts contexts) : base(contexts.core)
        {
            _contexts = contexts;
        }

        protected override ICollector<CoreEntity> GetTrigger(IContext<CoreEntity> context)
        {
            return context.CreateCollector(CoreMatcher.AllOf(CoreMatcher.Upgrade, CoreMatcher.UpgradeLevel,
                CoreMatcher.UpgradeTargetId));
        }

        protected override bool Filter(CoreEntity entity)
        {
            return entity.isUpgrade && entity.hasUpgradeLevel;
        }

        protected override void Execute(List<CoreEntity> entities)
        {
            foreach (var coreEntity in entities)
            {
                if (coreEntity.upgradeTargetId.value == default)
                {
                    continue;
                }

                var upgradeData = GameStaticData.GetUpgradeData(coreEntity.upgradeId.value);


                var baseValue = 0f;
                var multiplierValue = 0f;


                var values = new Dictionary<GameEnums.StatId, StatData>();
                foreach (var stat in upgradeData.GetStatsByLevel(0))
                {
                    if (values.ContainsKey(stat.StatId) == false)
                    {
                        values.Add(stat.StatId, new StatData()
                        {
                            StatId = stat.StatId
                        });
                    }
                }


                for (int i = 1; i <= coreEntity.upgradeLevel.value; i++)
                {
                    foreach (var stat in upgradeData.GetStatsByLevel(i))
                    {
                        if (stat.IsAggregateValue)
                        {
                            values[stat.StatId].Base += stat.Base;
                            values[stat.StatId].Multiplier += stat.Multiplier;
                        }
                        else
                        {
                            values[stat.StatId].Base = stat.Base;
                            values[stat.StatId].Multiplier = stat.Multiplier;
                        }
                    }
                }


                foreach (var dict in values)
                {
                    var statData = dict.Value;

                    var statEntity = _contexts.core.GetEntitiesWithStatSource(coreEntity.coreId.value)
                        .SingleOrDefault(e => e.isStatModifier && e.statId.value == statData.StatId);

                    if (statEntity != null)
                    {
                        if (statEntity.statBase.value != statData.Base)
                        {
                            statEntity.ReplaceStatBase(statData.Base);
                        }

                        if (statEntity.statMultiplier.value != statData.Multiplier)
                        {
                            statEntity.ReplaceStatMultiplier(statData.Multiplier);
                        }
                    }
                    else
                    {
                        statEntity = _contexts.core.CreateEntity();
                        statEntity.isStatModifier = true;

                        statEntity.AddStatId(statData.StatId);
                        statEntity.AddStatBase(statData.Base);
                        statEntity.AddStatMultiplier(statData.Multiplier);
                        statEntity.AddStatSource(coreEntity.coreId.value);
                        statEntity.AddStatModifierTarget(coreEntity.upgradeTargetId.value);
                    }
                }
            }
        }
    }
}