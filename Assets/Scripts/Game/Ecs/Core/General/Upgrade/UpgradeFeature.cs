using Game.Ecs.Core.General.Upgrade.CreateUpgrades;
using Game.Ecs.Core.General.Upgrade.HandleUpgrades;

namespace Game.Ecs.Core.General.Upgrade
{
    public class UpgradeFeature : Feature
    {
        public UpgradeFeature(Contexts contexts)
        {
            Add(new HandleUpgradesFeature(contexts));
            Add(new HandleUpgradeStatModifiersSystem(contexts));
            Add(new CreateUpgradesFeature(contexts));
            Add(new UpdateValidUpgradesSystem(contexts));
        }
    }
}