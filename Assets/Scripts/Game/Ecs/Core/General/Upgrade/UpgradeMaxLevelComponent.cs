using Entitas;

namespace Game.Ecs.Core.General.Upgrade
{
    [Core]
    public class UpgradeMaxLevelComponent : IComponent
    {
        public int value;
    }
}