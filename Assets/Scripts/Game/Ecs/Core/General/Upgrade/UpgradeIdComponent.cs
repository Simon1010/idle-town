using Entitas;
using Entitas.CodeGeneration.Attributes;

namespace Game.Ecs.Core.General.Upgrade
{
    [Core]
    public class UpgradeIdComponent : IComponent
    {
        [EntityIndex] public GameEnums.UpgradeId value;
    }
}