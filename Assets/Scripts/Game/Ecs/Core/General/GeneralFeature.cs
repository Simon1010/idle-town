using Game.Ecs.Core.General.Destroy;
using Game.Ecs.Core.General.Gestures;
using Game.Ecs.Core.General.Movement;
using Game.Ecs.Core.General.Stat;

namespace Game.Ecs.Core.General
{
    public class GeneralFeature : Feature
    {
        public GeneralFeature(Contexts contexts)
        {
            Add(new MovementFeature(contexts));
            Add(new StatFeature(contexts));
            Add(new GestureFeature(contexts));
        }
    }
}