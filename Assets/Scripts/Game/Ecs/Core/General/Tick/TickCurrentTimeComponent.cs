using Entitas;

namespace Game.Ecs.Core.General.Tick
{
    [Core]
    public class TickCurrentTimeComponent : IComponent
    {
        public float value;
    }
}