namespace Game.Ecs.Core.General.Tick
{
    public class TickFeature : Feature
    {
        public TickFeature(Contexts contexts)
        {
            Add(new ResetTickSystem(contexts));
            Add(new CountdownTickSystem(contexts));
        }
    }
}