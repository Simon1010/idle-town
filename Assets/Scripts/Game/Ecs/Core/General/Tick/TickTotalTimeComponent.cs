using Entitas;

namespace Game.Ecs.Core.General.Tick
{
    [Core]
    public class TickTotalTimeComponent : IComponent
    {
        public float value;
    }
}