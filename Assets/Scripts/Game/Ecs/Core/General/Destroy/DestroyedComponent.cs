using Entitas;
using IdleMineGenerator.CodeGeneration.Attributes;

namespace Game.Ecs.Core.General.Destroy
{
    [Core, Command, InputAction, ListenerAny, ListenerSelf]
    public class DestroyedComponent : IComponent
    {
    }
}