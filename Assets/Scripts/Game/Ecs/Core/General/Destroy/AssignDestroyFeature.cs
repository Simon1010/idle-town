using Game.Ecs.Core.General.Collision;

namespace Game.Ecs.Core.General.Destroy
{
    public class AssignDestroyFeature : Feature
    {
        public AssignDestroyFeature(Contexts contexts)
        {
            Add(new RemoveCollisionSystem(contexts));
            Add(new DestroyChildenIfParentDestroyedSystem(contexts));
        }
    }
}