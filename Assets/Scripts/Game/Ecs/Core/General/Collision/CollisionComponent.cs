using Entitas;

namespace Game.Ecs.Core.General.Collision
{
    [Core]
    public class CollisionComponent : IComponent
    {
        public CoreEntityId entityA;
        public CoreEntityId entityB;
    }
}