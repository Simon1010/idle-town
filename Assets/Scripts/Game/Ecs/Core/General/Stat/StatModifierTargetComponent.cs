using Entitas;
using Entitas.CodeGeneration.Attributes;

namespace Game.Ecs.Core.General.Stat
{
    [Core]
    public class StatModifierTargetComponent : IComponent
    {
        [EntityIndex] public CoreEntityId value;
    }
}