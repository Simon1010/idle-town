using Entitas;
using IdleMineGenerator.CodeGeneration.Attributes;

namespace Game.Ecs.Core.General.Stat.Stats
{
    [Core, ListenerSelf, ListenerAny]
    public class StatSpeedComponent : IComponent
    {
        public float value;
    }
}