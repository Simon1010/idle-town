using Entitas;

namespace Game.Ecs.Core.General.Stat
{
    [Core]
    public class StatMultiplierComponent : IComponent
    {
        public float value;
    }
}