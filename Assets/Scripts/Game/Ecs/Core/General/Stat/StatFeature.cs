namespace Game.Ecs.Core.General.Stat
{
    public class StatFeature : Feature
    {
        public StatFeature(Contexts contexts)
        {
            Add(new HandleStatChangeSystem(contexts));
        }
    }
}