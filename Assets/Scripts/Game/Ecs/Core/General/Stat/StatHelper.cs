using System;
using System.Linq;

namespace Game.Ecs.Core.General.Stat
{
    public static class StatHelper
    {
        public static void CreateStats(Contexts contexts, CoreEntity entity, StatData[] statData)
        {
            foreach (var data in statData)
            {
                var stat = GameStaticData.GetStatData(entity, data.StatId);
                SetStatValue(entity, data.StatId, stat.Base * (1 + data.Multiplier));
            }
        }

        public static void UpdateStat(Contexts contexts, CoreEntity target, GameEnums.StatId statId)
        {
            var statModifiers = contexts.core.GetEntitiesWithStatModifierTarget(target.coreId.value)
                .Where(e => e.isStatModifier);

            var baseModifiers = statModifiers.Sum(e => e.statBase.value);
            var multiplierModifiers = statModifiers.Sum(e => e.statMultiplier.value);

            var stat = GameStaticData.GetStatData(target, statId);
            var baseValue = stat.Base;
            var multiplierValue = stat.Multiplier;

            baseValue += baseModifiers;
            multiplierValue += multiplierModifiers;

            var statValue = baseValue * (1 + multiplierValue);
            SetStatValue(target, statId, statValue);
        }

        private static void SetStatValue(CoreEntity target, GameEnums.StatId statId, float statValue)
        {
            switch (statId)
            {
                case GameEnums.StatId.Speed:
                    target.ReplaceStatSpeed(statValue);
                    break;
                case GameEnums.StatId.TotalWorkers:
                    target.ReplaceStatTotalWorkers(statValue);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(statId), statId, null);
            }
        }
    }

    public class StatData
    {
        public GameEnums.StatId StatId;
        public float Base;
        public float Multiplier;
    }
}