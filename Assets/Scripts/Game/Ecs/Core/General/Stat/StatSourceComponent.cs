using Entitas;
using Entitas.CodeGeneration.Attributes;

namespace Game.Ecs.Core.General.Stat
{
    [Core]
    public class StatSourceComponent : IComponent
    {
        [EntityIndex] public CoreEntityId value;
    }
}