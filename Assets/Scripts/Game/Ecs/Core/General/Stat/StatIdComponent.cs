using Entitas;

namespace Game.Ecs.Core.General.Stat
{
    [Core]
    public class StatIdComponent : IComponent
    {
        public GameEnums.StatId value;
    }
}