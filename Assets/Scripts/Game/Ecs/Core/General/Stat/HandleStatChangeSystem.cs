using System.Collections.Generic;
using Entitas;
using UnityEngine;

namespace Game.Ecs.Core.General.Stat
{
    public class HandleStatChangeSystem : ReactiveSystem<CoreEntity>
    {
        private readonly Contexts _contexts;

        public HandleStatChangeSystem(Contexts contexts) : base(contexts.core)
        {
            _contexts = contexts;
        }

        protected override ICollector<CoreEntity> GetTrigger(IContext<CoreEntity> context)
        {
            return context.CreateCollector(CoreMatcher.AllOf(CoreMatcher.StatModifier)
                .AnyOf(CoreMatcher.StatBase, CoreMatcher.StatMultiplier));
        }

        protected override bool Filter(CoreEntity entity)
        {
            return entity.isStatModifier;
        }

        protected override void Execute(List<CoreEntity> entities)
        {
            foreach (var coreEntity in entities)
            {
                var target = _contexts.core.GetEntityWithCoreId(coreEntity.statModifierTarget.value);
                StatHelper.UpdateStat(_contexts, target, coreEntity.statId.value);
            }
        }
    }
}