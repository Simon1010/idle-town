using Entitas;
using Entitas.CodeGeneration.Attributes;
using IdleMineGenerator.CodeGeneration.Attributes;

namespace Game.Ecs.Core.General
{
    [Core, ListenerAny]
    public class ParentComponent : IComponent
    {
        [EntityIndex] public CoreEntityId value;
    }
}