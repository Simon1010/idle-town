using Entitas;

namespace Game.Ecs.Core.General.Gestures
{
    public class DestroyLingeringTapsSystem : ICleanupSystem
    {
        private readonly Contexts _contexts;
        private IGroup<CoreEntity> _group;

        public DestroyLingeringTapsSystem(Contexts contexts)
        {
            _contexts = contexts;
            _group = contexts.core.GetGroup(CoreMatcher.Tap);
        }

        public void Cleanup()
        {
            foreach (var coreEntity in _group.GetEntities())
            {
                coreEntity.isDestroyed = true;
            }
        }
    }
}