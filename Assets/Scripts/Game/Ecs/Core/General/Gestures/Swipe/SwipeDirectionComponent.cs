using System.Numerics;
using Entitas;

namespace Game.Ecs.Core.General.Gestures.Swipe
{
    [Core]
    public class SwipeDirectionComponent : IComponent
    {
        public Vector2 value;
    }
}