using Entitas;
using UnityEngine;

namespace Game.Ecs.Core.General.Gestures.Swipe
{
    public class DestroyLingeringSwipeGesturesSystem : ICleanupSystem
    {
        private readonly Contexts _contexts;
        private IGroup<CoreEntity> _group;

        public DestroyLingeringSwipeGesturesSystem(Contexts contexts)
        {
            _contexts = contexts;
            _group = contexts.core.GetGroup(CoreMatcher.Swipe);
        }

        public void Cleanup()
        {
            foreach (var coreEntity in _group.GetEntities())
            {
                coreEntity.isDestroyed = true;
            }
        }
    }
}