using Game.Ecs.Core.General.Gestures.Swipe;

namespace Game.Ecs.Core.General.Gestures
{
    public class GestureFeature : Feature
    {
        public GestureFeature(Contexts contexts)
        {
            Add(new DestroyLingeringTapsSystem(contexts));
            Add(new DestroyLingeringSwipeGesturesSystem(contexts));
        }
    }
}