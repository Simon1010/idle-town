using Entitas;

namespace Game.Ecs.Core.General.Movement
{
    [Core]
    public class MovementPathCurrentNodeIdComponent : IComponent
    {
        public int value;
    }
}