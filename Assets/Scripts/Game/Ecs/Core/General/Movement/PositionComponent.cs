using System.Numerics;
using Entitas;
using IdleMineGenerator.CodeGeneration.Attributes;

namespace Game.Ecs.Core.General.Movement
{
    [Core, ListenerSelf()]
    public class PositionComponent : IComponent
    {
        public Vector3 value;
    }
}