using System.Numerics;
using Entitas;

namespace Game.Ecs.Core.General.Movement
{
    [Core]
    public class VelocityComponent : IComponent
    {
        public Vector3 value;
    }
}