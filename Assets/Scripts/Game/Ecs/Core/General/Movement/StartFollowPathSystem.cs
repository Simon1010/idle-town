using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using Entitas;

namespace Game.Ecs.Core.General.Movement
{
    public class StartFollowPathSystem : ReactiveSystem<CoreEntity>
    {
        private readonly Contexts _contexts;

        public StartFollowPathSystem(Contexts contexts) : base(contexts.core)
        {
            _contexts = contexts;
        }

        protected override ICollector<CoreEntity> GetTrigger(IContext<CoreEntity> context)
        {
            return context.CreateCollector(CoreMatcher.MovementPath);
        }

        protected override bool Filter(CoreEntity entity)
        {
            return entity.hasMovementPath;
        }

        protected override void Execute(List<CoreEntity> entities)
        {
            foreach (var coreEntity in entities)
            {
                var closestNode = Enumerable.OrderBy<Vector3, float>(coreEntity.movementPath.value, e =>
                    Vector3.Distance(coreEntity.position.value, e)).First();

                var closestNodeIndex = Enumerable.ToList<Vector3>(coreEntity.movementPath.value)
                    .IndexOf(closestNode);

                if (coreEntity.hasMovementPathCurrentNodeId)
                {
                    closestNodeIndex = coreEntity.movementPathCurrentNodeId.value;
                }

                coreEntity.ReplaceMovementPathCurrentNodeId(closestNodeIndex);
                coreEntity.ReplaceMoveTo(coreEntity.movementPath.value[closestNodeIndex]);
            }
        }
    }
}