using System.Numerics;
using Entitas;

namespace Game.Ecs.Core.General.Movement
{
    public class MoveToSystem : IExecuteSystem, ICleanupSystem
    {
        private readonly Contexts _contexts;
        private IGroup<CoreEntity> _group;
        private IGroup<CoreEntity> _completeGroup;

        public MoveToSystem(Contexts contexts)
        {
            _contexts = contexts;
            _group = _contexts.core.GetGroup(CoreMatcher.MoveTo);
            _completeGroup = _contexts.core.GetGroup(CoreMatcher.MoveToComplete);
        }

        public void Execute()
        {
            foreach (var coreEntity in _group.GetEntities())
            {
                var distance = Vector3.Distance(coreEntity.position.value, coreEntity.moveTo.value);
                var direction = Vector3.Normalize(coreEntity.moveTo.value - coreEntity.position.value);

                var moveSpeed = coreEntity.moveSpeed.value;

                if (moveSpeed >= distance)
                {
                    moveSpeed = distance;
                }

                if (distance > coreEntity.moveToArriveDistance.value)
                {
                    coreEntity.ReplaceVelocity(moveSpeed * direction);
                }
                else
                {
                    coreEntity.ReplacePosition(coreEntity.moveTo.value);
                    coreEntity.RemoveMoveTo();
                    if (coreEntity.hasVelocity)
                    {
                        coreEntity.RemoveVelocity();
                    }

                    coreEntity.isMoveToComplete = true;
                }
            }
        }

        public void Cleanup()
        {
            foreach (var coreEntity in _completeGroup.GetEntities())
            {
                coreEntity.isMoveToComplete = false;
            }
        }
    }
}