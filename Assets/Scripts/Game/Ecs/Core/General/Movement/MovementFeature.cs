using Game.Ecs.Core.General.Movement.MimicPosition;

namespace Game.Ecs.Core.General.Movement
{
    public class MovementFeature : Feature
    {
        public MovementFeature(Contexts contexts)
        {
            Add(new VelocityFeature(contexts));
            Add(new MimicPositionSystem(contexts));
            Add(new CompleteFollowPathSystem(contexts));
            Add(new MoveToSystem(contexts));
            Add(new StartFollowPathSystem(contexts));
            Add(new FollowPathSystem(contexts));
        }
    }
}