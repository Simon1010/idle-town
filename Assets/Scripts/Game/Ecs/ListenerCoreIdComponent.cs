﻿using Entitas;

namespace Game.Ecs
{
    [Listener]
    public class ListenerCoreIdComponent : IComponent
    {
        public CoreEntityId value;
    }
}
