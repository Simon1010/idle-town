using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using Game.Data;
using Game.Data.SaveData;
using UnityEngine;

namespace Game
{
    public static class GameSaveManager
    {
        public static GameSaveData CreateSaveGame(Contexts contexts)
        {
            var saveData = new GameSaveData();

//Save stuff
            return saveData;
        }


        public static void SaveGame(Contexts contexts)
        {
            var saveGame = CreateSaveGame(contexts);
            var bf = new BinaryFormatter();
            var fs = File.Open(Application.persistentDataPath + "/GameSave.dat", FileMode.OpenOrCreate);
            bf.Serialize(fs, saveGame);
            fs.Close();
        }

        public static GameSaveData GetSaveData(Contexts contexts)
        {
            if (File.Exists(Application.persistentDataPath + "/GameSave.dat"))
            {
                var fs = File.Open(Application.persistentDataPath + "/GameSave.dat", FileMode.Open);
                var bf = new BinaryFormatter();
                var saveData = (GameSaveData) bf.Deserialize(fs);
                fs.Close();
                return saveData;
            }

            return null;
        }

        public static void LoadGame(Contexts contexts, GameSaveData gameSave)
        {
            //setup loaded game
        }
    }
}