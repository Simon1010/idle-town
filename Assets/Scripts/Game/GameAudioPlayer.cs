using System;
using UnityEngine;

namespace ZombieGame
{
    public class GameAudioPlayer : MonoBehaviour
    {
        public bool IsMusic;
        private AudioSource _audioSource;

        private void Awake()
        {
            _audioSource = GetComponent<AudioSource>();
        }

        private void Start()
        {
            if (IsMusic)
            {
                _audioSource.loop = true;
            }
        }

        private void Update()
        {
            if (IsMusic)
            {
                _audioSource.mute = GameSoundManager.Instance.IsMusicMuted;
            }
            else
            {
                _audioSource.mute = GameSoundManager.Instance.IsSFXMuted;
            }
        }
    }
}