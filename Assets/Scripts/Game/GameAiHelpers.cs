using System.Numerics;

namespace Game
{
    public static class GameAiHelpers
    {
        public static bool Move(CoreEntity coreEntity, Vector3 position, float threshold = 5f)
        {
            var distance = Vector3.Distance(coreEntity.position.value, position);
            if (distance > threshold)
            {
                if (coreEntity.hasMoveTo == false)
                {
                    coreEntity.ReplaceMoveToArriveDistance(threshold);
//                    coreEntity.ReplaceMoveTo(position);
                }

                return false;
            }

            return true;
        }

        public static bool IsAt(CoreEntity coreEntity, Vector3 position)
        {
            var distance = Vector3.Distance(coreEntity.position.value, position);
            return distance < 5f;
        }

        public static void CancelPath(CoreEntity coreEntity)
        {
            if (coreEntity.hasMovementPath)
                coreEntity.RemoveMovementPath();
            if (coreEntity.hasMoveTo)
                coreEntity.RemoveMoveTo();
        }

        public static void StartPath(CoreEntity coreEntity, Vector3[] pathNodes, float threshold = 5f)
        {
            coreEntity.ReplaceMoveToArriveDistance(threshold);
            coreEntity.AddMovementPath(pathNodes);
        }

        public static void StopMoving(CoreEntity coreEntity)
        {
            if (coreEntity.hasMoveTo)
            {
                coreEntity.RemoveMoveTo();
            }
        }

        public static bool IsMoving(CoreEntity coreEntity)
        {
            return coreEntity.hasMoveTo;
        }
    }
}