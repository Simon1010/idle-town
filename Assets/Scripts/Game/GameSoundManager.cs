using UnityEngine;
using Utilities;

namespace ZombieGame
{
    public class GameSoundManager : MonoBehaviour
    {
        private static GameSoundManager _instance;

        public static GameSoundManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = GameObject.Find("Main").GetComponentInChildren<GameSoundManager>();
                }

                return _instance;
            }
        }

        public bool IsMusicMuted;
        public bool IsSFXMuted;

        public void ToggleSFX()
        {
            SetSFX(!IsSFXMuted);
        }

        public void SetSFX(bool value)
        {
            IsSFXMuted = value;
        }

        public void ToggleMusic()
        {
            SetMusic(!IsMusicMuted);
        }

        public void SetMusic(bool value)
        {
            IsMusicMuted = value;
        }

        public void Play(string audioClip, bool isMusic = false)
        {
            var audioPlayer =
                GameObject.Instantiate(GameResourceManager.Instance.GetPrefab<GameAudioPlayer>("AudioPlayer"),
                    transform);

            audioPlayer.IsMusic = isMusic;
            
            var audioSource = audioPlayer.GetComponent<AudioSource>();
            var clip = GameResourceManager.Instance.GetSound(audioClip);
            audioSource.clip = clip;
            audioSource.Play();
            audioPlayer.gameObject.AddComponent<DestroyAfter>().Initialize(clip.length);
        }
    }
}