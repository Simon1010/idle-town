using System.Numerics;

namespace Game
{
    public static class GameSettings
    {
        public const float Friction = 0.92f;
        public const float Gravity = 0.98f;

        public static readonly Vector3 PlotSize = new Vector3(10, 0, 10);
    }
}