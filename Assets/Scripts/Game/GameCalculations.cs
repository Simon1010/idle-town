using System;
using System.Data;
using Game.Data;

namespace Game
{
    public static class GameCalculations
    {

        public static float ToSeconds(float i)
        {
            return i / UnityEngine.Time.deltaTime;
        }

        public static float CalculateStat(StatData statData, int level)
        {
            var value = statData.Base;
            if (level > 0)
            {
                value *= (float) Math.Pow(statData.Multiplier, level);
            }

            return value;
        }

        public static int CalculateStatInt(StatData statData, int level)
        {
            return (int) CalculateStat(statData, level);
        }
    }
}