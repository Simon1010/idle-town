//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentLookupGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public static class ListenerComponentsLookup {

    public const int ListenerCoreId = 0;
    public const int ListenerAnyBuilding = 1;
    public const int ListenerAnyBuildingPlot = 2;
    public const int ListenerAnyDestroyed = 3;
    public const int ListenerAnyParent = 4;
    public const int ListenerAnyPlayer = 5;
    public const int ListenerAnyPlayerMoney = 6;
    public const int ListenerAnyPlot = 7;
    public const int ListenerAnyStatSpeed = 8;
    public const int ListenerAnyUpgrade = 9;
    public const int ListenerAnyUpgradeLevel = 10;
    public const int ListenerAnyUpgradeValid = 11;
    public const int ListenerDestroyed = 12;
    public const int ListenerPlayerCurrentPlot = 13;
    public const int ListenerPlayerMoney = 14;
    public const int ListenerPlayerZoom = 15;
    public const int ListenerPosition = 16;
    public const int ListenerStatSpeed = 17;
    public const int ListenerUpgradeLevel = 18;

    public const int TotalComponents = 19;

    public static readonly string[] componentNames = {
        "ListenerCoreId",
        "ListenerAnyBuilding",
        "ListenerAnyBuildingPlot",
        "ListenerAnyDestroyed",
        "ListenerAnyParent",
        "ListenerAnyPlayer",
        "ListenerAnyPlayerMoney",
        "ListenerAnyPlot",
        "ListenerAnyStatSpeed",
        "ListenerAnyUpgrade",
        "ListenerAnyUpgradeLevel",
        "ListenerAnyUpgradeValid",
        "ListenerDestroyed",
        "ListenerPlayerCurrentPlot",
        "ListenerPlayerMoney",
        "ListenerPlayerZoom",
        "ListenerPosition",
        "ListenerStatSpeed",
        "ListenerUpgradeLevel"
    };

    public static readonly System.Type[] componentTypes = {
        typeof(Game.Ecs.ListenerCoreIdComponent),
        typeof(ListenerAnyBuildingComponent),
        typeof(ListenerAnyBuildingPlotComponent),
        typeof(ListenerAnyDestroyedComponent),
        typeof(ListenerAnyParentComponent),
        typeof(ListenerAnyPlayerComponent),
        typeof(ListenerAnyPlayerMoneyComponent),
        typeof(ListenerAnyPlotComponent),
        typeof(ListenerAnyStatSpeedComponent),
        typeof(ListenerAnyUpgradeComponent),
        typeof(ListenerAnyUpgradeLevelComponent),
        typeof(ListenerAnyUpgradeValidComponent),
        typeof(ListenerDestroyedComponent),
        typeof(ListenerPlayerCurrentPlotComponent),
        typeof(ListenerPlayerMoneyComponent),
        typeof(ListenerPlayerZoomComponent),
        typeof(ListenerPositionComponent),
        typeof(ListenerStatSpeedComponent),
        typeof(ListenerUpgradeLevelComponent)
    };
}
