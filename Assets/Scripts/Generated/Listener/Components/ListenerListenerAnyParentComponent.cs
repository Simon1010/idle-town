//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentEntityApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class ListenerEntity {

    public ListenerAnyParentComponent listenerAnyParent { get { return (ListenerAnyParentComponent)GetComponent(ListenerComponentsLookup.ListenerAnyParent); } }
    public bool hasListenerAnyParent { get { return HasComponent(ListenerComponentsLookup.ListenerAnyParent); } }

    public void AddListenerAnyParent(IListenerAnyParent newValue) {
        var index = ListenerComponentsLookup.ListenerAnyParent;
        var component = (ListenerAnyParentComponent)CreateComponent(index, typeof(ListenerAnyParentComponent));
        component.value = newValue;
        AddComponent(index, component);
    }

    public void ReplaceListenerAnyParent(IListenerAnyParent newValue) {
        var index = ListenerComponentsLookup.ListenerAnyParent;
        var component = (ListenerAnyParentComponent)CreateComponent(index, typeof(ListenerAnyParentComponent));
        component.value = newValue;
        ReplaceComponent(index, component);
    }

    public void RemoveListenerAnyParent() {
        RemoveComponent(ListenerComponentsLookup.ListenerAnyParent);
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentMatcherApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public sealed partial class ListenerMatcher {

    static Entitas.IMatcher<ListenerEntity> _matcherListenerAnyParent;

    public static Entitas.IMatcher<ListenerEntity> ListenerAnyParent {
        get {
            if (_matcherListenerAnyParent == null) {
                var matcher = (Entitas.Matcher<ListenerEntity>)Entitas.Matcher<ListenerEntity>.AllOf(ListenerComponentsLookup.ListenerAnyParent);
                matcher.componentNames = ListenerComponentsLookup.componentNames;
                _matcherListenerAnyParent = matcher;
            }

            return _matcherListenerAnyParent;
        }
    }
}
