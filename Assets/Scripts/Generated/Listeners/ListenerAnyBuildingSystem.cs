//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by IdleMineGenerator.CodeGeneration.Plugins.ListenerGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System.Collections.Generic;
using Entitas;
    public class ListenerAnyBuildingSystem : ReactiveSystem<CoreEntity>
    {
        private readonly Contexts contexts;

        public ListenerAnyBuildingSystem(Contexts contexts) : base(contexts.core)
        {
            this.contexts = contexts;
        }

        protected override void Execute(List<CoreEntity> entities)
        {
            var listeners = contexts.listener.GetEntities(ListenerMatcher.ListenerAnyBuilding);
            foreach (var coreEntity in entities)
            {
                foreach (var listenerEntity in listeners)
                {
                        listenerEntity.listenerAnyBuilding.value.OnAnyBuilding(coreEntity,coreEntity.isBuilding);
                    }
                }
            }


        protected override bool Filter(CoreEntity entity)
        {
            return entity.isBuilding;
        }

        protected override ICollector<CoreEntity> GetTrigger(IContext<CoreEntity> context)
        {
            return context.CreateCollector(CoreMatcher.Building);
        }
    }

    public interface IListenerAnyBuilding
    {
        void OnAnyBuilding(CoreEntity coreEntity, bool isBuilding);
    }
