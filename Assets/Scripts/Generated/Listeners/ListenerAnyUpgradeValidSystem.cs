//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by IdleMineGenerator.CodeGeneration.Plugins.ListenerGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System.Collections.Generic;
using Entitas;
    public class ListenerAnyUpgradeValidSystem : ReactiveSystem<CoreEntity>
    {
        private readonly Contexts contexts;

        public ListenerAnyUpgradeValidSystem(Contexts contexts) : base(contexts.core)
        {
            this.contexts = contexts;
        }

        protected override void Execute(List<CoreEntity> entities)
        {
            var listeners = contexts.listener.GetEntities(ListenerMatcher.ListenerAnyUpgradeValid);
            foreach (var coreEntity in entities)
            {
                foreach (var listenerEntity in listeners)
                {
                        listenerEntity.listenerAnyUpgradeValid.value.OnAnyUpgradeValid(coreEntity,coreEntity.isUpgradeValid);
                    }
                }
            }


        protected override bool Filter(CoreEntity entity)
        {
            return entity.isUpgradeValid;
        }

        protected override ICollector<CoreEntity> GetTrigger(IContext<CoreEntity> context)
        {
            return context.CreateCollector(CoreMatcher.UpgradeValid);
        }
    }

    public interface IListenerAnyUpgradeValid
    {
        void OnAnyUpgradeValid(CoreEntity coreEntity, bool isUpgradeValid);
    }
