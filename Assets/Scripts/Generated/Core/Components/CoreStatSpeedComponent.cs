//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentEntityApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class CoreEntity {

    public Game.Ecs.Core.General.Stat.Stats.StatSpeedComponent statSpeed { get { return (Game.Ecs.Core.General.Stat.Stats.StatSpeedComponent)GetComponent(CoreComponentsLookup.StatSpeed); } }
    public bool hasStatSpeed { get { return HasComponent(CoreComponentsLookup.StatSpeed); } }

    public void AddStatSpeed(float newValue) {
        var index = CoreComponentsLookup.StatSpeed;
        var component = (Game.Ecs.Core.General.Stat.Stats.StatSpeedComponent)CreateComponent(index, typeof(Game.Ecs.Core.General.Stat.Stats.StatSpeedComponent));
        component.value = newValue;
        AddComponent(index, component);
    }

    public void ReplaceStatSpeed(float newValue) {
        var index = CoreComponentsLookup.StatSpeed;
        var component = (Game.Ecs.Core.General.Stat.Stats.StatSpeedComponent)CreateComponent(index, typeof(Game.Ecs.Core.General.Stat.Stats.StatSpeedComponent));
        component.value = newValue;
        ReplaceComponent(index, component);
    }

    public void RemoveStatSpeed() {
        RemoveComponent(CoreComponentsLookup.StatSpeed);
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentMatcherApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public sealed partial class CoreMatcher {

    static Entitas.IMatcher<CoreEntity> _matcherStatSpeed;

    public static Entitas.IMatcher<CoreEntity> StatSpeed {
        get {
            if (_matcherStatSpeed == null) {
                var matcher = (Entitas.Matcher<CoreEntity>)Entitas.Matcher<CoreEntity>.AllOf(CoreComponentsLookup.StatSpeed);
                matcher.componentNames = CoreComponentsLookup.componentNames;
                _matcherStatSpeed = matcher;
            }

            return _matcherStatSpeed;
        }
    }
}
