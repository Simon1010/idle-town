//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentEntityApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class CoreEntity {

    public Game.Ecs.Core.General.ParentComponent parent { get { return (Game.Ecs.Core.General.ParentComponent)GetComponent(CoreComponentsLookup.Parent); } }
    public bool hasParent { get { return HasComponent(CoreComponentsLookup.Parent); } }

    public void AddParent(Game.Ecs.CoreEntityId newValue) {
        var index = CoreComponentsLookup.Parent;
        var component = (Game.Ecs.Core.General.ParentComponent)CreateComponent(index, typeof(Game.Ecs.Core.General.ParentComponent));
        component.value = newValue;
        AddComponent(index, component);
    }

    public void ReplaceParent(Game.Ecs.CoreEntityId newValue) {
        var index = CoreComponentsLookup.Parent;
        var component = (Game.Ecs.Core.General.ParentComponent)CreateComponent(index, typeof(Game.Ecs.Core.General.ParentComponent));
        component.value = newValue;
        ReplaceComponent(index, component);
    }

    public void RemoveParent() {
        RemoveComponent(CoreComponentsLookup.Parent);
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentMatcherApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public sealed partial class CoreMatcher {

    static Entitas.IMatcher<CoreEntity> _matcherParent;

    public static Entitas.IMatcher<CoreEntity> Parent {
        get {
            if (_matcherParent == null) {
                var matcher = (Entitas.Matcher<CoreEntity>)Entitas.Matcher<CoreEntity>.AllOf(CoreComponentsLookup.Parent);
                matcher.componentNames = CoreComponentsLookup.componentNames;
                _matcherParent = matcher;
            }

            return _matcherParent;
        }
    }
}
